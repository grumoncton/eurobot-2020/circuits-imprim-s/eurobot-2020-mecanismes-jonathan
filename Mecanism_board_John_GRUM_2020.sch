<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="no"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="no"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="PM_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="PF_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="WFL_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="no"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="silktop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="no"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="no"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="no"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Tensy_4.0">
<packages>
<package name="TENSY_4.0">
<wire x1="0" y1="0" x2="0" y2="17.81" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="35.56" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="17.81" x2="35.56" y2="17.81" width="0.127" layer="21"/>
<wire x1="35.56" y1="17.78" x2="35.56" y2="0" width="0.127" layer="21"/>
<pad name="P$1" x="1.27" y="1.27" drill="1.016"/>
<pad name="P$2" x="3.81" y="1.27" drill="1.016"/>
<pad name="P$3" x="6.35" y="1.27" drill="1.016"/>
<pad name="P$4" x="8.89" y="1.27" drill="1.016"/>
<pad name="P$5" x="11.43" y="1.27" drill="1.016"/>
<pad name="P$6" x="13.97" y="1.27" drill="1.016"/>
<pad name="P$7" x="16.51" y="1.27" drill="1.016"/>
<pad name="P$8" x="19.05" y="1.27" drill="1.016"/>
<pad name="P$9" x="21.59" y="1.27" drill="1.016"/>
<pad name="P$10" x="24.13" y="1.27" drill="1.016"/>
<pad name="P$11" x="26.67" y="1.27" drill="1.016"/>
<pad name="P$12" x="29.21" y="1.27" drill="1.016"/>
<pad name="P$13" x="31.75" y="1.27" drill="1.016"/>
<pad name="P$14" x="34.29" y="1.27" drill="1.016"/>
<pad name="P$15" x="1.27" y="16.51" drill="1.016"/>
<pad name="P$16" x="3.81" y="16.51" drill="1.016"/>
<pad name="P$17" x="6.35" y="16.51" drill="1.016"/>
<pad name="P$18" x="8.89" y="16.51" drill="1.016"/>
<pad name="P$19" x="11.43" y="16.51" drill="1.016"/>
<pad name="P$20" x="13.97" y="16.51" drill="1.016"/>
<pad name="P$21" x="16.51" y="16.51" drill="1.016"/>
<pad name="P$22" x="19.05" y="16.51" drill="1.016"/>
<pad name="P$23" x="21.59" y="16.51" drill="1.016"/>
<pad name="P$24" x="24.13" y="16.51" drill="1.016"/>
<pad name="P$25" x="26.67" y="16.51" drill="1.016"/>
<pad name="P$26" x="29.21" y="16.51" drill="1.016"/>
<pad name="P$27" x="31.75" y="16.51" drill="1.016"/>
<pad name="P$28" x="34.29" y="16.51" drill="1.016"/>
<pad name="P$29" x="34.29" y="3.81" drill="1.016"/>
<pad name="P$30" x="34.29" y="6.35" drill="1.016"/>
<pad name="P$31" x="34.29" y="8.89" drill="1.016"/>
<pad name="P$32" x="34.29" y="11.43" drill="1.016"/>
<pad name="P$33" x="34.29" y="13.97" drill="1.016"/>
<pad name="P$34" x="3.81" y="13.97" drill="1.016"/>
</package>
</packages>
<symbols>
<symbol name="TENSY_4.0">
<wire x1="17.78" y1="-12.7" x2="17.78" y2="38.1" width="0.254" layer="94"/>
<wire x1="17.78" y1="-12.7" x2="0" y2="-12.7" width="0.254" layer="94"/>
<wire x1="0" y1="-12.7" x2="-0.03" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-12.7" x2="0" y2="38.1" width="0.254" layer="94"/>
<wire x1="0" y1="38.1" x2="17.78" y2="38.1" width="0.254" layer="94"/>
<pin name="GND" x="-2.54" y="35.56" visible="pin" length="short" direction="pwr"/>
<pin name="0" x="-2.54" y="33.02" visible="pin" length="short"/>
<pin name="1" x="-2.54" y="30.48" visible="pin" length="short"/>
<pin name="2" x="-2.54" y="27.94" visible="pin" length="short"/>
<pin name="3" x="-2.54" y="25.4" visible="pin" length="short"/>
<pin name="4" x="-2.54" y="22.86" visible="pin" length="short"/>
<pin name="5" x="-2.54" y="20.32" visible="pin" length="short"/>
<pin name="6" x="-2.54" y="17.78" visible="pin" length="short"/>
<pin name="7" x="-2.54" y="15.24" visible="pin" length="short"/>
<pin name="8" x="-2.54" y="12.7" visible="pin" length="short"/>
<pin name="10" x="-2.54" y="7.62" visible="pin" length="short"/>
<pin name="11" x="-2.54" y="5.08" visible="pin" length="short"/>
<pin name="12" x="-2.54" y="2.54" visible="pin" length="short"/>
<pin name="3.3V" x="20.32" y="30.48" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="VIN" x="20.32" y="35.56" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="14" x="20.32" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="9" x="-2.54" y="10.16" visible="pin" length="short"/>
<pin name="15" x="20.32" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="16" x="20.32" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="17" x="20.32" y="12.7" visible="pin" length="short" rot="R180"/>
<pin name="18" x="20.32" y="15.24" visible="pin" length="short" rot="R180"/>
<pin name="19" x="20.32" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="20" x="20.32" y="20.32" visible="pin" length="short" rot="R180"/>
<pin name="21" x="20.32" y="22.86" visible="pin" length="short" rot="R180"/>
<pin name="22" x="20.32" y="25.4" visible="pin" length="short" rot="R180"/>
<pin name="23" x="20.32" y="27.94" visible="pin" length="short" rot="R180"/>
<pin name="13" x="20.32" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="GND_3" x="20.32" y="33.02" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="ON/OFF" x="-2.54" y="0" visible="pin" length="short" direction="in"/>
<pin name="PROGRAM" x="-2.54" y="-2.54" visible="pin" length="short" direction="in"/>
<pin name="GND_2" x="-2.54" y="-5.08" visible="pin" length="short" direction="pwr"/>
<pin name="3.3V_2" x="-2.54" y="-7.62" visible="pin" length="short" direction="pwr"/>
<pin name="VBAT" x="-2.54" y="-10.16" visible="pin" length="short" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TENSY_4.0" prefix="TENSY_4.0">
<gates>
<gate name="G$1" symbol="TENSY_4.0" x="-40.64" y="2.54"/>
</gates>
<devices>
<device name="" package="TENSY_4.0">
<connects>
<connect gate="G$1" pin="0" pad="P$2"/>
<connect gate="G$1" pin="1" pad="P$3"/>
<connect gate="G$1" pin="10" pad="P$12"/>
<connect gate="G$1" pin="11" pad="P$13"/>
<connect gate="G$1" pin="12" pad="P$14"/>
<connect gate="G$1" pin="13" pad="P$28"/>
<connect gate="G$1" pin="14" pad="P$27"/>
<connect gate="G$1" pin="15" pad="P$26"/>
<connect gate="G$1" pin="16" pad="P$25"/>
<connect gate="G$1" pin="17" pad="P$24"/>
<connect gate="G$1" pin="18" pad="P$23"/>
<connect gate="G$1" pin="19" pad="P$22"/>
<connect gate="G$1" pin="2" pad="P$4"/>
<connect gate="G$1" pin="20" pad="P$21"/>
<connect gate="G$1" pin="21" pad="P$20"/>
<connect gate="G$1" pin="22" pad="P$19"/>
<connect gate="G$1" pin="23" pad="P$18"/>
<connect gate="G$1" pin="3" pad="P$5"/>
<connect gate="G$1" pin="3.3V" pad="P$17"/>
<connect gate="G$1" pin="3.3V_2" pad="P$30"/>
<connect gate="G$1" pin="4" pad="P$6"/>
<connect gate="G$1" pin="5" pad="P$7"/>
<connect gate="G$1" pin="6" pad="P$8"/>
<connect gate="G$1" pin="7" pad="P$9"/>
<connect gate="G$1" pin="8" pad="P$10"/>
<connect gate="G$1" pin="9" pad="P$11"/>
<connect gate="G$1" pin="GND" pad="P$1"/>
<connect gate="G$1" pin="GND_2" pad="P$31"/>
<connect gate="G$1" pin="GND_3" pad="P$16"/>
<connect gate="G$1" pin="ON/OFF" pad="P$33"/>
<connect gate="G$1" pin="PROGRAM" pad="P$32"/>
<connect gate="G$1" pin="VBAT" pad="P$29"/>
<connect gate="G$1" pin="VIN" pad="P$15"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="PulseOximeterECG_AU" urn="urn:adsk.eagle:library:14846661">
<description>Generated from &lt;b&gt;PulseOximeterECG_AU.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:14846670/1" library_version="3">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:14846728/1" prefix="GND" library_version="3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="292133-3">
<packages>
<package name="TE_292133-3">
<wire x1="-5.6" y1="-3.8" x2="-5.6" y2="2" width="0.127" layer="51"/>
<wire x1="-5.6" y1="2" x2="2.2" y2="2" width="0.127" layer="51"/>
<wire x1="2.2" y1="2" x2="2.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-3.8" x2="-5.6" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-5.6" y1="-3.8" x2="-5.6" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-5.6" y1="-3.8" x2="2.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="2.2" y1="-3.8" x2="2.2" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-5.6" y1="-3.8" x2="-5.6" y2="2" width="0.127" layer="21"/>
<wire x1="2.2" y1="2" x2="2.2" y2="-3.8" width="0.127" layer="21"/>
<wire x1="2.2" y1="2" x2="-5.6" y2="2" width="0.127" layer="21"/>
<wire x1="2.2" y1="-3.8" x2="-5.6" y2="-3.8" width="0.127" layer="21"/>
<wire x1="-5.85" y1="-4.05" x2="-5.85" y2="2.25" width="0.05" layer="39"/>
<wire x1="-5.85" y1="2.25" x2="2.45" y2="2.25" width="0.05" layer="39"/>
<wire x1="2.45" y1="2.25" x2="2.45" y2="-4.05" width="0.05" layer="39"/>
<wire x1="2.45" y1="-4.05" x2="-5.85" y2="-4.05" width="0.05" layer="39"/>
<text x="-6.6" y="3.25" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.6" y="-6.05" size="1.27" layer="27">&gt;VALUE</text>
<circle x="2.85" y="0" radius="0.1" width="0.2" layer="21"/>
<circle x="2.85" y="0" radius="0.1" width="0.2" layer="51"/>
<pad name="1" x="0" y="0" drill="0.85" diameter="1.2" shape="square"/>
<pad name="2" x="-2" y="0" drill="0.85" diameter="1.2"/>
<pad name="3" x="-4" y="0" drill="0.85" diameter="1.2"/>
</package>
</packages>
<symbols>
<symbol name="292133-3">
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="-5.58" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-10.16" y="2.54" length="middle" direction="pas"/>
<pin name="2" x="-10.16" y="0" length="middle" direction="pas"/>
<pin name="3" x="-10.16" y="-2.54" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="292133-3" prefix="J">
<gates>
<gate name="G$1" symbol="292133-3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TE_292133-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="COMMENT" value="292133-3"/>
<attribute name="DESCRIPTION" value=" AMP CT Series; 2mm Pitch 3 Way 1 Row Straight PCB Header; Solder Termination "/>
<attribute name="DIGI-KEY_PART_NUMBER" value="455-1833-ND"/>
<attribute name="DIGI-KEY_PURCHASE_URL" value="https://www.digikey.ca/products/en?keywords=B03B-PASK-1(LF)(SN)"/>
<attribute name="EU_ROHS_COMPLIANCE" value="Compliant"/>
<attribute name="MF" value="JST Sales America Inc."/>
<attribute name="MP" value="B03B-PASK-1(LF)(SN)"/>
<attribute name="MPN" value="B03B-PASK-1(LF)(SN)" constant="no"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="TE_PURCHASE_URL" value="https://www.te.com/usa-en/product-292133-3.html?te_bu=Cor&amp;te_type=disp&amp;te_campaign=seda_glo_cor-seda-global-disp-prtnr-fy19-seda-model-bom-cta_sma-317_1&amp;elqCampaignId=32493"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X03">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X3">
<description>&lt;h3&gt;PTH - 3 Pin Vertical Molex Polarized Header&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/01932.pdf"&gt; 4UCONN part # 01932 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<text x="1.143" y="2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0.889" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="2.159" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-4.572" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Locking Footprint&lt;/h3&gt;
Pins are staggered 0.005" off center to lock pins while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Long Pad w/ Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center to lock pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.1176" x2="6.0706" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.8636" x2="6.0706" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<text x="-1.27" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X3_LOCK">
<description>&lt;h3&gt;PTH - 3 Pin Vertical Molex Polarized Header&lt;/h3&gt;
Pins are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/01932.pdf"&gt; 4UCONN part # 01932 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<text x="1.143" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0.889" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH Locking&lt;/h3&gt;
Holes are offset 0.007" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7.1778" y="0" drill="1.2" diameter="2.032"/>
<text x="2.032" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="1.905" y="-4.699" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-PTH">
<description>&lt;h3&gt;JST 3 Pin Right Angle Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_PP_HOLES_ONLY">
<description>&lt;h3&gt;Pogo Pins - 3 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-5MM-3">
<description>&lt;h3&gt;Screw Terminal  5mm Pitch -3 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 5mm/197mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.1" y1="4.2" x2="13.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="4.2" x2="13.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="13.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="13.1" y1="4" x2="13.7" y2="4" width="0.2032" layer="51"/>
<wire x1="13.7" y1="4" x2="13.7" y2="3" width="0.2032" layer="51"/>
<wire x1="13.7" y1="3" x2="13.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.413" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.413"/>
<pad name="3" x="10" y="0" drill="1.3" diameter="2.413"/>
<text x="3.683" y="2.794" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.429" y="1.905" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin Locking Footprint w/out Silk Outline&lt;/h3&gt;
Holes are offset from center 0.005" to lock pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-SMD">
<description>&lt;h3&gt;JST 3 Pin Right Angle SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.99" y1="-2.07" x2="-4.99" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.99" y1="-5.57" x2="-4.19" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-5.57" x2="-4.19" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-3.07" x2="-2.99" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="-3.07" x2="4.21" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-3.07" x2="4.21" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-5.57" x2="5.01" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="5.01" y1="-5.57" x2="5.01" y2="-2.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="1.93" x2="-2.99" y2="1.93" width="0.2032" layer="21"/>
<smd name="3" x="-1.99" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="1" x="2.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-4.39" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="4.41" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="0.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<text x="-1.397" y="0.635" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03-1MM-RA">
<description>&lt;h3&gt;Plated Through Hole - 3 Pin SMD&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1" y1="-4.6" x2="1" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="-0.35" width="0.254" layer="21"/>
<wire x1="1.75" y1="-0.35" x2="2.4997" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.4997" y1="-0.35" x2="2.4997" y2="-2" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-0.35" x2="-1.75" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="0" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="1" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_FEMALE">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Female Header&lt;/h3&gt;
Silk outline of pin location
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.935" y1="4.25" x2="-3.935" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="4.25" x2="-3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="-4.25" x2="3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-3.935" y1="-4.25" x2="3.935" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-0.32" y1="6.8" x2="0.32" y2="7.65" layer="51"/>
<rectangle x1="2.22" y1="6.8" x2="2.86" y2="7.65" layer="51"/>
<rectangle x1="-2.86" y1="6.8" x2="-2.22" y2="7.65" layer="51"/>
<smd name="3" x="2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="0" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-1.524" y="0.889" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE">
<description>&lt;h3&gt;SMD- 3 Pin Right Angle Male Headers&lt;/h3&gt;
No silk outline, but tDocu layer shows pin location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="0" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<text x="-1.524" y="0.254" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE_POST">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Male Header w/ Alignment Posts&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt; 4UCONN part # 11026 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.6"/>
<hole x="1.27" y="0" drill="1.6"/>
<text x="-1.397" y="1.524" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-3-PTH-VERT">
<description>&lt;h3&gt;JST 3 Pin Vertical Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-2.25" x2="-3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="2.25" x2="3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="2.25" x2="3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.75" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.75" size="1.27" layer="51">-</text>
<text x="1.7" y="0.95" size="0.8" layer="51">S</text>
<text x="-1.397" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLER">
<description>&lt;h3&gt;SMD - 3 Pin Right Angle Male Header w/ Alignment Posts&lt;/h3&gt;
&lt;p&gt;&lt;b&gt;Datasheet referenced for footprint:&lt;/b&gt;&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt; 4UCONN part # 11026 &lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3589"/>
<hole x="1.27" y="0" drill="1.3589"/>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLEST">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3462"/>
<hole x="1.27" y="0" drill="1.3462"/>
</package>
<package name="JST-3-PTH-NS">
<description>&lt;h3&gt;JST 3 Pin Right Angle Plated Through Hole &amp;ndash; NO SILK&lt;/h3&gt;
&lt;p&gt;No silkscreen outline. tDoc layer (51) indicates connector footprint.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:3&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6"/>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-3-NS">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -3 Pin PTH &amp;ndash; NO SILK&lt;/h3&gt;
&lt;p&gt;No silkscreen outline. tDoc layer (51) indicates connector footprint.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 3&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="8.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.4" x2="8.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-2.8" x2="8.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="8.75" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.75" y1="3.15" x2="9.25" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="3.15" x2="9.25" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.15" x2="8.75" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="2.159" y="3.683" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-4.572" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="3.15" x2="5.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD">
<description>&lt;h3&gt;JST-Right Angle Male Header SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;li&gt;JST_2MM_MALE&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.397" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="0.635" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_BIG">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.15"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD-VERT">
<description>&lt;h3&gt;JST-Vertical Male Header SMT &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="-3.81" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-3.81" y="2.21" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
</package>
<package name="SCREWTERMINAL-5MM-2">
<description>&lt;h3&gt;Screw Terminal  5mm Pitch -2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 5mm/197mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<description>&lt;h3&gt;Plated Through Hole - Locking Footprint&lt;/h3&gt;
Holes are staggered by 0.005" from center to hold pins while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2_LOCK">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole Locking Footprint&lt;/h3&gt;
Holes are offset from center by 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - Long Pads with Locking Footprint&lt;/h3&gt;
Pins are staggered by 0.005" from center to hold pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH Locking&lt;/h3&gt;
Holes are offset from center 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - Long Pads without Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.397" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="2.73" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.6" x2="-2.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.6" x2="2.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="1X02_XTRA_BIG">
<description>&lt;h3&gt;Plated Through Hole - 0.1" holes&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.2"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
<text x="-5.08" y="2.667" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-5.08" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_PP_HOLES_ONLY">
<description>&lt;h3&gt;Pogo Pins Connector - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole- No Silk&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; No silk outline of connector. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole - KIT&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad.
&lt;br&gt; This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<description>&lt;h3&gt;Spring Terminal- PCB Mount 2 Pin PTH&lt;/h3&gt;
tDocu marks the spring arms
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/SpringTerminal.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<description>&lt;h3&gt;2 Pin Screw Terminal - 2.54mm&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<text x="-1.27" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_RA_PTH_FEMALE">
<wire x1="-2.79" y1="4.25" x2="-2.79" y2="-4.25" width="0.1778" layer="21"/>
<text x="-1.397" y="0.762" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="2.79" y1="4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="4.25" x2="2.79" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="-4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<pad name="2" x="-1.27" y="-5.85" drill="0.8"/>
<pad name="1" x="1.27" y="-5.85" drill="0.8"/>
</package>
<package name="1X02_POKEHOME">
<description>2 pin poke-home connector

part number 2062-2P from STA</description>
<wire x1="-7" y1="-4" x2="-7" y2="2" width="0.2032" layer="21"/>
<wire x1="-7" y1="2" x2="-7" y2="4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="4" x2="4.7" y2="-4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="-4" x2="-7" y2="-4" width="0.2032" layer="21"/>
<smd name="P2" x="5.25" y="-2" dx="3.5" dy="2" layer="1"/>
<smd name="P1" x="5.25" y="2" dx="3.5" dy="2" layer="1"/>
<smd name="P4" x="-4" y="-2" dx="6" dy="2" layer="1"/>
<smd name="P3" x="-4" y="2" dx="6" dy="2" layer="1"/>
<wire x1="-7" y1="4" x2="4.7" y2="4" width="0.2032" layer="21"/>
<text x="0.635" y="-3.175" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.635" y="-1.905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_03">
<description>&lt;h3&gt;3 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CONN_02">
<description>&lt;h3&gt;2 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_03" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13875"&gt; Stackable Header - 3 Pin (Female, 0.1")&lt;/a&gt; (PRT-13875)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08232 with associated crimp pins and housings.</description>
<gates>
<gate name="J$1" symbol="CONN_03" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XXX-00000" constant="no"/>
<attribute name="VALUE" value="455-1750-1-ND" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08625" constant="no"/>
<attribute name="SF_ID" value="PRT-08096" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08288" constant="no"/>
<attribute name="SF_ID" value="PRT-08235" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X03_LOCK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X03_LOCK_LONGPADS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X3_LOCK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08625" constant="no"/>
<attribute name="SF_ID" value="PRT-08096" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08288" constant="no"/>
<attribute name="SF_ID" value="PRT-08235" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_NO_SILK" package="1X03_NO_SILK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X03_LONGPADS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-3-PTH">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="WIRE-10037" constant="no"/>
<attribute name="SF_ID" value="PRT-09915" constant="no"/>
</technology>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X03_PP_HOLES_ONLY">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SCREW-5MM" package="SCREWTERMINAL-5MM-3">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10134" constant="no"/>
<attribute name="SF_SKU" value="PRT-08433" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_NO_SILK" package="1X03_LOCK_NO_SILK">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-SMD" package="JST-3-SMD">
<connects>
<connect gate="J$1" pin="1" pad="3"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-12591" constant="no"/>
<attribute name="VALUE" value="3-PIN SMD" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="1X03-1MM-RA">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X03_SMD_RA_FEMALE">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10926"/>
<attribute name="VALUE" value="1x3 RA Female .1&quot;"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE" package="1X03_SMD_RA_MALE">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10925"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE_POST" package="1X03_SMD_RA_MALE_POST">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-3-PTH-VERT">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13230" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLER" package="1X03_SMD_RA_MALE_POST_SMALLER">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11912" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLEST" package="1X03_SMD_RA_MALE_POST_SMALLEST">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-NS" package="JST-3-PTH-NS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW-NS" package="SCREWTERMINAL-3.5MM-3-NS">
<connects>
<connect gate="J$1" pin="1" pad="1"/>
<connect gate="J$1" pin="2" pad="2"/>
<connect gate="J$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_02" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08432</description>
<gates>
<gate name="G$1" symbol="CONN_02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_SKU" value="PRT-08432" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09863" constant="no"/>
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALL_POKEHOME" package="1X02_POKEHOME">
<connects>
<connect gate="G$1" pin="1" pad="P1 P3"/>
<connect gate="G$1" pin="2" pad="P2 P4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13512"/>
</technology>
</technologies>
</device>
<device name="PTH_RA_FEMALE" package="1X02_RA_PTH_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13700"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DRV8800PWPR">
<packages>
<package name="SOP65P640X120-17N">
<wire x1="-2.2606" y1="2.1336" x2="-2.2606" y2="2.4384" width="0" layer="51"/>
<wire x1="-2.2606" y1="2.4384" x2="-3.302" y2="2.4384" width="0" layer="51"/>
<wire x1="-3.302" y1="2.4384" x2="-3.302" y2="2.1336" width="0" layer="51"/>
<wire x1="-3.302" y1="2.1336" x2="-2.2606" y2="2.1336" width="0" layer="51"/>
<wire x1="-2.2606" y1="1.4732" x2="-2.2606" y2="1.778" width="0" layer="51"/>
<wire x1="-2.2606" y1="1.778" x2="-3.302" y2="1.778" width="0" layer="51"/>
<wire x1="-3.302" y1="1.778" x2="-3.302" y2="1.4732" width="0" layer="51"/>
<wire x1="-3.302" y1="1.4732" x2="-2.2606" y2="1.4732" width="0" layer="51"/>
<wire x1="-2.2606" y1="0.8128" x2="-2.2606" y2="1.1176" width="0" layer="51"/>
<wire x1="-2.2606" y1="1.1176" x2="-3.302" y2="1.1176" width="0" layer="51"/>
<wire x1="-3.302" y1="1.1176" x2="-3.302" y2="0.8128" width="0" layer="51"/>
<wire x1="-3.302" y1="0.8128" x2="-2.2606" y2="0.8128" width="0" layer="51"/>
<wire x1="-2.2606" y1="0.1778" x2="-2.2606" y2="0.4826" width="0" layer="51"/>
<wire x1="-2.2606" y1="0.4826" x2="-3.302" y2="0.4826" width="0" layer="51"/>
<wire x1="-3.302" y1="0.4826" x2="-3.302" y2="0.1778" width="0" layer="51"/>
<wire x1="-3.302" y1="0.1778" x2="-2.2606" y2="0.1778" width="0" layer="51"/>
<wire x1="-2.2606" y1="-0.4826" x2="-2.2606" y2="-0.1778" width="0" layer="51"/>
<wire x1="-2.2606" y1="-0.1778" x2="-3.302" y2="-0.1778" width="0" layer="51"/>
<wire x1="-3.302" y1="-0.1778" x2="-3.302" y2="-0.4826" width="0" layer="51"/>
<wire x1="-3.302" y1="-0.4826" x2="-2.2606" y2="-0.4826" width="0" layer="51"/>
<wire x1="-2.2606" y1="-1.1176" x2="-2.2606" y2="-0.8128" width="0" layer="51"/>
<wire x1="-2.2606" y1="-0.8128" x2="-3.302" y2="-0.8128" width="0" layer="51"/>
<wire x1="-3.302" y1="-0.8128" x2="-3.302" y2="-1.1176" width="0" layer="51"/>
<wire x1="-3.302" y1="-1.1176" x2="-2.2606" y2="-1.1176" width="0" layer="51"/>
<wire x1="-2.2606" y1="-1.778" x2="-2.2606" y2="-1.4732" width="0" layer="51"/>
<wire x1="-2.2606" y1="-1.4732" x2="-3.302" y2="-1.4732" width="0" layer="51"/>
<wire x1="-3.302" y1="-1.4732" x2="-3.302" y2="-1.778" width="0" layer="51"/>
<wire x1="-3.302" y1="-1.778" x2="-2.2606" y2="-1.778" width="0" layer="51"/>
<wire x1="-2.2606" y1="-2.4384" x2="-2.2606" y2="-2.1336" width="0" layer="51"/>
<wire x1="-2.2606" y1="-2.1336" x2="-3.302" y2="-2.1336" width="0" layer="51"/>
<wire x1="-3.302" y1="-2.1336" x2="-3.302" y2="-2.4384" width="0" layer="51"/>
<wire x1="-3.302" y1="-2.4384" x2="-2.2606" y2="-2.4384" width="0" layer="51"/>
<wire x1="2.2606" y1="-2.1336" x2="2.2606" y2="-2.4384" width="0" layer="51"/>
<wire x1="2.2606" y1="-2.4384" x2="3.302" y2="-2.4384" width="0" layer="51"/>
<wire x1="3.302" y1="-2.4384" x2="3.302" y2="-2.1336" width="0" layer="51"/>
<wire x1="3.302" y1="-2.1336" x2="2.2606" y2="-2.1336" width="0" layer="51"/>
<wire x1="2.2606" y1="-1.4732" x2="2.2606" y2="-1.778" width="0" layer="51"/>
<wire x1="2.2606" y1="-1.778" x2="3.302" y2="-1.778" width="0" layer="51"/>
<wire x1="3.302" y1="-1.778" x2="3.302" y2="-1.4732" width="0" layer="51"/>
<wire x1="3.302" y1="-1.4732" x2="2.2606" y2="-1.4732" width="0" layer="51"/>
<wire x1="2.2606" y1="-0.8128" x2="2.2606" y2="-1.1176" width="0" layer="51"/>
<wire x1="2.2606" y1="-1.1176" x2="3.302" y2="-1.1176" width="0" layer="51"/>
<wire x1="3.302" y1="-1.1176" x2="3.302" y2="-0.8128" width="0" layer="51"/>
<wire x1="3.302" y1="-0.8128" x2="2.2606" y2="-0.8128" width="0" layer="51"/>
<wire x1="2.2606" y1="-0.1778" x2="2.2606" y2="-0.4826" width="0" layer="51"/>
<wire x1="2.2606" y1="-0.4826" x2="3.302" y2="-0.4826" width="0" layer="51"/>
<wire x1="3.302" y1="-0.4826" x2="3.302" y2="-0.1778" width="0" layer="51"/>
<wire x1="3.302" y1="-0.1778" x2="2.2606" y2="-0.1778" width="0" layer="51"/>
<wire x1="2.2606" y1="0.4826" x2="2.2606" y2="0.1778" width="0" layer="51"/>
<wire x1="2.2606" y1="0.1778" x2="3.302" y2="0.1778" width="0" layer="51"/>
<wire x1="3.302" y1="0.1778" x2="3.302" y2="0.4826" width="0" layer="51"/>
<wire x1="3.302" y1="0.4826" x2="2.2606" y2="0.4826" width="0" layer="51"/>
<wire x1="2.2606" y1="1.1176" x2="2.2606" y2="0.8128" width="0" layer="51"/>
<wire x1="2.2606" y1="0.8128" x2="3.302" y2="0.8128" width="0" layer="51"/>
<wire x1="3.302" y1="0.8128" x2="3.302" y2="1.1176" width="0" layer="51"/>
<wire x1="3.302" y1="1.1176" x2="2.2606" y2="1.1176" width="0" layer="51"/>
<wire x1="2.2606" y1="1.778" x2="2.2606" y2="1.4732" width="0" layer="51"/>
<wire x1="2.2606" y1="1.4732" x2="3.302" y2="1.4732" width="0" layer="51"/>
<wire x1="3.302" y1="1.4732" x2="3.302" y2="1.778" width="0" layer="51"/>
<wire x1="3.302" y1="1.778" x2="2.2606" y2="1.778" width="0" layer="51"/>
<wire x1="2.2606" y1="2.4384" x2="2.2606" y2="2.1336" width="0" layer="51"/>
<wire x1="2.2606" y1="2.1336" x2="3.302" y2="2.1336" width="0" layer="51"/>
<wire x1="3.302" y1="2.1336" x2="3.302" y2="2.4384" width="0" layer="51"/>
<wire x1="3.302" y1="2.4384" x2="2.2606" y2="2.4384" width="0" layer="51"/>
<wire x1="-2.2606" y1="-2.54" x2="2.2606" y2="-2.54" width="0" layer="51"/>
<wire x1="2.2606" y1="-2.54" x2="2.2606" y2="2.54" width="0" layer="51"/>
<wire x1="2.2606" y1="2.54" x2="0.3048" y2="2.54" width="0" layer="51"/>
<wire x1="0.3048" y1="2.54" x2="-0.3048" y2="2.54" width="0" layer="51"/>
<wire x1="-0.3048" y1="2.54" x2="-2.2606" y2="2.54" width="0" layer="51"/>
<wire x1="-2.2606" y1="2.54" x2="-2.2606" y2="-2.54" width="0" layer="51"/>
<wire x1="0.3048" y1="2.54" x2="-0.3048" y2="2.54" width="0" layer="51" curve="-180"/>
<wire x1="-1.8796" y1="-2.54" x2="1.8796" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.8796" y1="2.54" x2="0.3048" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.54" x2="-0.3048" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.54" x2="-1.8796" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.54" x2="-0.3048" y2="2.54" width="0.1524" layer="21" curve="-180"/>
<text x="-3.45466875" y="3.175240625" size="2.082959375" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.45688125" y="-5.08365" size="2.084290625" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<smd name="1" x="-2.921" y="2.286" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="2" x="-2.921" y="1.6256" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="3" x="-2.921" y="0.9652" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="4" x="-2.921" y="0.3302" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="5" x="-2.921" y="-0.3302" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="6" x="-2.921" y="-0.9652" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="7" x="-2.921" y="-1.6256" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="8" x="-2.921" y="-2.286" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="9" x="2.921" y="-2.286" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="10" x="2.921" y="-1.6256" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="11" x="2.921" y="-0.9652" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="12" x="2.921" y="-0.3302" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="13" x="2.921" y="0.3302" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="14" x="2.921" y="0.9652" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="15" x="2.921" y="1.6256" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="16" x="2.921" y="2.286" dx="1.4732" dy="0.3556" layer="1"/>
<smd name="17" x="0" y="0" dx="2.9972" dy="2.9972" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="DRV8800PWPR">
<wire x1="-12.7" y1="25.4" x2="-12.7" y2="-30.48" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-30.48" x2="12.7" y2="-30.48" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-30.48" x2="12.7" y2="25.4" width="0.4064" layer="94"/>
<wire x1="12.7" y1="25.4" x2="-12.7" y2="25.4" width="0.4064" layer="94"/>
<text x="-5.369859375" y="29.4961" size="2.08686875" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-2.54108125" y="-36.2612" size="2.083690625" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="VBB" x="-17.78" y="20.32" length="middle" direction="pwr"/>
<pin name="ENABLE" x="-17.78" y="15.24" length="middle" direction="in"/>
<pin name="SENSE" x="-17.78" y="12.7" length="middle" direction="in"/>
<pin name="NSLEEP" x="-17.78" y="10.16" length="middle" direction="in"/>
<pin name="MODE" x="-17.78" y="7.62" length="middle" direction="in"/>
<pin name="PHASE" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="CP1" x="-17.78" y="0" length="middle" direction="pas"/>
<pin name="CP2" x="-17.78" y="-2.54" length="middle" direction="pas"/>
<pin name="VCP" x="-17.78" y="-7.62" length="middle" direction="pas"/>
<pin name="VREG" x="-17.78" y="-10.16" length="middle" direction="pas"/>
<pin name="NC" x="-17.78" y="-15.24" length="middle" direction="nc"/>
<pin name="EPAD" x="-17.78" y="-17.78" length="middle" direction="pas"/>
<pin name="GND_2" x="-17.78" y="-22.86" length="middle" direction="pas"/>
<pin name="GND" x="-17.78" y="-25.4" length="middle" direction="pas"/>
<pin name="NFAULT" x="17.78" y="20.32" length="middle" direction="out" rot="R180"/>
<pin name="OUT+" x="17.78" y="17.78" length="middle" direction="out" rot="R180"/>
<pin name="OUT-" x="17.78" y="15.24" length="middle" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DRV8800PWPR" prefix="U">
<description>DMOS FULL-BRIDGE MOTOR DRIVERS</description>
<gates>
<gate name="A" symbol="DRV8800PWPR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P640X120-17N">
<connects>
<connect gate="A" pin="CP1" pad="11"/>
<connect gate="A" pin="CP2" pad="12"/>
<connect gate="A" pin="ENABLE" pad="6"/>
<connect gate="A" pin="EPAD" pad="17"/>
<connect gate="A" pin="GND" pad="13"/>
<connect gate="A" pin="GND_2" pad="4"/>
<connect gate="A" pin="MODE" pad="2"/>
<connect gate="A" pin="NC" pad="16"/>
<connect gate="A" pin="NFAULT" pad="1"/>
<connect gate="A" pin="NSLEEP" pad="5"/>
<connect gate="A" pin="OUT+" pad="7"/>
<connect gate="A" pin="OUT-" pad="10"/>
<connect gate="A" pin="PHASE" pad="3"/>
<connect gate="A" pin="SENSE" pad="8"/>
<connect gate="A" pin="VBB" pad="9"/>
<connect gate="A" pin="VCP" pad="14"/>
<connect gate="A" pin="VREG" pad="15"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value=" Bipolar Motor Driver DMOS Parallel 16-HTSSOP "/>
<attribute name="DIGI-KEY_PART_NUMBER" value="296-25377-1-ND"/>
<attribute name="DIGI-KEY_PURCHASE_URL" value="https://www.digikey.ca/product-detail/en/texas-instruments/DRV8800PWPR/296-25377-1-ND/2183148?utm_source=snapeda&amp;utm_medium=aggregator&amp;utm_campaign=symbol"/>
<attribute name="MF" value="Texas Instruments"/>
<attribute name="MP" value="DRV8800PWPR"/>
<attribute name="PACKAGE" value="HTSSOP-16 Texas Instruments"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BeagleBone_Blue_R3" urn="urn:adsk.eagle:library:5828899">
<description>Generated from &lt;b&gt;BeagleBone_Blue.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
<package name="0402-CAP" urn="urn:adsk.eagle:footprint:5829087/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.092" y1="0.483" x2="1.092" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.092" y1="0.483" x2="1.092" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.092" y1="-0.483" x2="-1.092" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.092" y1="-0.483" x2="-1.092" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.5588" y="0" dx="0.6096" dy="0.508" layer="1"/>
<smd name="2" x="0.5588" y="0" dx="0.6096" dy="0.508" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="C0603" urn="urn:adsk.eagle:footprint:5829372/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.127" layer="21"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805" urn="urn:adsk.eagle:footprint:8076988/1">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="0402-CAP" urn="urn:adsk.eagle:package:5829530/3" type="model">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<packageinstances>
<packageinstance name="0402-CAP"/>
</packageinstances>
</package3d>
<package3d name="C0603" urn="urn:adsk.eagle:package:5829814/2" type="model">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<packageinstances>
<packageinstance name="C0603"/>
</packageinstances>
</package3d>
<package3d name="0805" urn="urn:adsk.eagle:package:5829534/4" type="model">
<packageinstances>
<packageinstance name="0805"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CAPACITOR-N" urn="urn:adsk.eagle:symbol:5828961/1" library_version="49">
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR-N" urn="urn:adsk.eagle:component:5829897/5" prefix="C" library_version="49">
<gates>
<gate name="G$1" symbol="CAPACITOR-N" x="0" y="-2.54"/>
</gates>
<devices>
<device name="0402" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829530/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829814/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829534/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Wurth_Elektronik_Passive_Capacitors_rev18b" urn="urn:adsk.eagle:library:7562178">
<description>&lt;BR&gt;Wurth Elektronik - Electrolytic Capacitors,Ceramic Capacitors &amp; Film Capacitors&lt;br&gt;&lt;Hr&gt;

&lt;BR&gt;
&lt;TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0&gt;
&lt;TR&gt;   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;&lt;br&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt; &lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------------------------&lt;BR&gt;
&lt;B&gt;&lt;I&gt;&lt;span style='font-size:26pt;
  color:#FF6600;'&gt;WE &lt;/span&gt;&lt;/i&gt;&lt;/b&gt;
&lt;BR&gt;
      ---------------------------&lt;BR&gt;&lt;b&gt;Würth Elektronik&lt;/b&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;

  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  
&lt;/TABLE&gt;
&lt;B&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;More than you expect&lt;BR&gt;&lt;BR&gt;&lt;BR&gt;&lt;/B&gt;

&lt;HR&gt;&lt;BR&gt;
&lt;b&gt;Würth Elektronik eiSos GmbH &amp; Co. KG&lt;/b&gt;&lt;br&gt;
EMC &amp; Inductive Solutions&lt;br&gt;

Max-Eyth-Str.1&lt;br&gt;
D-74638 Waldenburg&lt;br&gt;
&lt;br&gt;
Tel: +49 (0)7942-945-0&lt;br&gt;
Fax:+49 (0)7942-945-5000&lt;br&gt;
&lt;br&gt;
&lt;a href="http://www.we-online.com/web/en/electronic_components/produkte_pb/bauteilebibliotheken/eagle_4.php"&gt;www.we-online.com/eagle&lt;/a&gt;&lt;br&gt;
&lt;a href="mailto:libraries@we-online.com"&gt;libraries@we-online.com&lt;/a&gt; &lt;BR&gt;&lt;BR&gt;
&lt;br&gt;&lt;HR&gt;&lt;BR&gt;
Neither Autodesk nor Würth Elektronik eiSos does warrant that this library is error-free or &lt;br&gt;
that it meets your specific requirements.&lt;br&gt;&lt;BR&gt;
Please contact us for more information.&lt;br&gt;&lt;BR&gt;&lt;br&gt;

Eagle Version 6, Library Revision 2018a, 2018-11-15&lt;br&gt;
&lt;HR&gt;
Copyright: Würth Elektronik</description>
<packages>
<package name="2.6X1.6X1(BXCXA)" urn="urn:adsk.eagle:footprint:7562201/1">
<smd name="2" x="-1.8" y="0" dx="2.6" dy="1.6" layer="1"/>
<smd name="1" x="1.8" y="0" dx="2.6" dy="1.6" layer="1"/>
<wire x1="-2.15" y1="2.15" x2="1.35" y2="2.15" width="0.127" layer="21"/>
<wire x1="-2.15" y1="-2.15" x2="-2.15" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-2.15" y1="-2.15" x2="1.35" y2="-2.15" width="0.127" layer="21"/>
<wire x1="-2.15" y1="2.15" x2="-2.15" y2="1.1" width="0.127" layer="21"/>
<wire x1="1.35" y1="2.15" x2="2.15" y2="1.35" width="0.127" layer="21"/>
<wire x1="1.35" y1="-2.15" x2="2.15" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-3.35" y1="2.4" x2="3.35" y2="2.4" width="0.127" layer="39"/>
<wire x1="-3.35" y1="-2.4" x2="3.35" y2="-2.4" width="0.127" layer="39"/>
<wire x1="-3.35" y1="2.4" x2="-3.35" y2="-2.4" width="0.127" layer="39"/>
<wire x1="3.35" y1="2.4" x2="3.35" y2="-2.4" width="0.127" layer="39"/>
<wire x1="2.15" y1="1.35" x2="2.15" y2="1" width="0.127" layer="21"/>
<wire x1="2.15" y1="-1.35" x2="2.15" y2="-1" width="0.127" layer="21"/>
<text x="-2.5" y="3.2" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.7" y="-4" size="1.016" layer="27">&gt;VALUE</text>
<text x="2.5" y="-2" size="1.016" layer="21">+</text>
</package>
<package name="3X1.6X1.4(BXCXA)" urn="urn:adsk.eagle:footprint:7562200/1">
<smd name="2" x="-2.2" y="0" dx="3" dy="1.6" layer="1"/>
<smd name="1" x="2.2" y="0" dx="3" dy="1.6" layer="1"/>
<wire x1="-2.65" y1="2.65" x2="1.65" y2="2.65" width="0.127" layer="21"/>
<wire x1="-2.65" y1="-2.65" x2="-2.65" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.65" y1="-2.65" x2="1.65" y2="-2.65" width="0.127" layer="21"/>
<wire x1="-2.65" y1="2.65" x2="-2.65" y2="1" width="0.127" layer="21"/>
<wire x1="1.65" y1="2.65" x2="2.65" y2="1.65" width="0.127" layer="21"/>
<wire x1="1.65" y1="-2.65" x2="2.65" y2="-1.65" width="0.127" layer="21"/>
<wire x1="-3.95" y1="2.9" x2="3.95" y2="2.9" width="0.127" layer="39"/>
<wire x1="-3.95" y1="-2.9" x2="3.95" y2="-2.9" width="0.127" layer="39"/>
<wire x1="-3.95" y1="2.9" x2="-3.95" y2="-2.9" width="0.127" layer="39"/>
<wire x1="3.95" y1="2.9" x2="3.95" y2="-2.9" width="0.127" layer="39"/>
<wire x1="2.65" y1="1.65" x2="2.65" y2="1.1" width="0.127" layer="21"/>
<wire x1="2.65" y1="-1.65" x2="2.65" y2="-1" width="0.127" layer="21"/>
<text x="-3" y="3.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.4" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
<text x="2.9" y="-2.2" size="1.27" layer="21">+</text>
</package>
<package name="3.5X1.6X2.1(BXCXA)" urn="urn:adsk.eagle:footprint:7562199/1">
<smd name="2" x="-2.8" y="0" dx="3.5" dy="1.6" layer="1"/>
<smd name="1" x="2.8" y="0" dx="3.5" dy="1.6" layer="1"/>
<wire x1="-3.3" y1="3.3" x2="1.75" y2="3.3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-1.1" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="1.75" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="-3.3" y2="1.1" width="0.127" layer="21"/>
<wire x1="1.75" y1="3.3" x2="3.3" y2="1.75" width="0.127" layer="21"/>
<wire x1="1.75" y1="-3.3" x2="3.3" y2="-1.75" width="0.127" layer="21"/>
<wire x1="-4.8" y1="3.55" x2="4.8" y2="3.55" width="0.127" layer="39"/>
<wire x1="-4.8" y1="-3.55" x2="4.8" y2="-3.55" width="0.127" layer="39"/>
<wire x1="-4.8" y1="3.55" x2="-4.8" y2="-3.55" width="0.127" layer="39"/>
<wire x1="4.8" y1="3.55" x2="4.8" y2="-3.55" width="0.127" layer="39"/>
<wire x1="3.3" y1="1.75" x2="3.3" y2="1.1" width="0.127" layer="21"/>
<wire x1="3.3" y1="-1.75" x2="3.3" y2="-1.1" width="0.127" layer="21"/>
<text x="-3.2" y="4.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.1" y="-5.2" size="1.27" layer="27">&gt;VALUE</text>
<text x="3.7" y="-2.4" size="1.27" layer="21">+</text>
</package>
<package name="4.2X1.9X2.8(BXCXA)" urn="urn:adsk.eagle:footprint:7562197/1">
<smd name="2" x="-3.5" y="0" dx="4.2" dy="1.9" layer="1"/>
<smd name="1" x="3.5" y="0" dx="4.2" dy="1.9" layer="1"/>
<wire x1="-4.15" y1="4.15" x2="2.35" y2="4.15" width="0.127" layer="21"/>
<wire x1="-4.15" y1="-4.15" x2="-4.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.15" y1="-4.15" x2="2.35" y2="-4.15" width="0.127" layer="21"/>
<wire x1="-4.15" y1="4.15" x2="-4.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="2.35" y1="4.15" x2="4.15" y2="2.35" width="0.127" layer="21"/>
<wire x1="2.35" y1="-4.15" x2="4.15" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-5.85" y1="4.4" x2="5.85" y2="4.4" width="0.127" layer="39"/>
<wire x1="-5.85" y1="-4.4" x2="5.85" y2="-4.4" width="0.127" layer="39"/>
<wire x1="-5.85" y1="4.4" x2="-5.85" y2="-4.4" width="0.127" layer="39"/>
<wire x1="5.85" y1="4.4" x2="5.85" y2="-4.4" width="0.127" layer="39"/>
<wire x1="4.15" y1="2.35" x2="4.15" y2="1.35" width="0.127" layer="21"/>
<wire x1="4.15" y1="-2.35" x2="4.15" y2="-1.15" width="0.127" layer="21"/>
<text x="-3.13" y="5.13" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.4" y="-6.33" size="1.27" layer="27">&gt;VALUE</text>
<text x="4.7" y="-3.3" size="1.27" layer="21">+</text>
</package>
<package name="4.4X1.9X4.3(BXCXA)" urn="urn:adsk.eagle:footprint:7562198/1">
<smd name="2" x="-4.35" y="0" dx="4.4" dy="1.9" layer="1"/>
<smd name="1" x="4.35" y="0" dx="4.4" dy="1.9" layer="1"/>
<wire x1="-5.15" y1="5.15" x2="3.15" y2="5.15" width="0.127" layer="21"/>
<wire x1="-5.15" y1="-5.15" x2="-5.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-5.15" y1="-5.15" x2="3.15" y2="-5.15" width="0.127" layer="21"/>
<wire x1="-5.15" y1="5.15" x2="-5.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="3.15" y1="5.15" x2="5.15" y2="3.15" width="0.127" layer="21"/>
<wire x1="3.15" y1="-5.15" x2="5.15" y2="-3.15" width="0.127" layer="21"/>
<wire x1="-6.8" y1="5.4" x2="6.8" y2="5.4" width="0.127" layer="39"/>
<wire x1="-6.8" y1="-5.4" x2="6.8" y2="-5.4" width="0.127" layer="39"/>
<wire x1="-6.8" y1="5.4" x2="-6.8" y2="-5.4" width="0.127" layer="39"/>
<wire x1="6.8" y1="5.4" x2="6.8" y2="-5.4" width="0.127" layer="39"/>
<wire x1="5.15" y1="3.15" x2="5.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="5.15" y1="-3.15" x2="5.15" y2="-1.35" width="0.127" layer="21"/>
<text x="-4.4" y="6.4" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.4" y="-7.6" size="1.27" layer="27">&gt;VALUE</text>
<text x="5.6" y="-2.6" size="1.27" layer="21">+</text>
</package>
<package name="5.8X2.5X4.3(BXCXA)" urn="urn:adsk.eagle:footprint:7562196/1">
<smd name="2" x="-5.05" y="0" dx="5.8" dy="2.5" layer="1"/>
<smd name="1" x="5.05" y="0" dx="5.8" dy="2.5" layer="1"/>
<wire x1="-6.75" y1="6.75" x2="4.25" y2="6.75" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-6.75" x2="-6.75" y2="-1.65" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-6.75" x2="4.25" y2="-6.75" width="0.127" layer="21"/>
<wire x1="-6.75" y1="6.75" x2="-6.75" y2="1.65" width="0.127" layer="21"/>
<wire x1="4.25" y1="6.75" x2="6.75" y2="4.25" width="0.127" layer="21"/>
<wire x1="4.25" y1="-6.75" x2="6.75" y2="-4.25" width="0.127" layer="21"/>
<wire x1="-8.2" y1="7" x2="8.2" y2="7" width="0.127" layer="39"/>
<wire x1="-8.2" y1="-7" x2="8.2" y2="-7" width="0.127" layer="39"/>
<wire x1="-8.2" y1="7" x2="-8.2" y2="-7" width="0.127" layer="39"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-7" width="0.127" layer="39"/>
<wire x1="6.75" y1="4.25" x2="6.75" y2="1.55" width="0.127" layer="21"/>
<wire x1="6.75" y1="-4.25" x2="6.75" y2="-1.75" width="0.127" layer="21"/>
<text x="-4.4" y="8.4" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.4" y="-9.6" size="1.27" layer="27">&gt;VALUE</text>
<text x="7.3" y="-4.1" size="1.27" layer="21">+</text>
</package>
<package name="6.5X3.5X6(BXCXA)" urn="urn:adsk.eagle:footprint:7562195/1">
<smd name="2" x="-6.25" y="0" dx="6.5" dy="3.5" layer="1"/>
<smd name="1" x="6.25" y="0" dx="6.5" dy="3.5" layer="1"/>
<wire x1="-8.55" y1="8.55" x2="6.05" y2="8.55" width="0.127" layer="21"/>
<wire x1="-8.55" y1="-8.55" x2="-8.55" y2="-2.15" width="0.127" layer="21"/>
<wire x1="-8.55" y1="-8.55" x2="6.05" y2="-8.55" width="0.127" layer="21"/>
<wire x1="-8.55" y1="8.55" x2="-8.55" y2="2.15" width="0.127" layer="21"/>
<wire x1="6.05" y1="8.55" x2="8.55" y2="6.05" width="0.127" layer="21"/>
<wire x1="6.05" y1="-8.55" x2="8.55" y2="-6.05" width="0.127" layer="21"/>
<wire x1="-9.75" y1="8.8" x2="9.7" y2="8.8" width="0.127" layer="39"/>
<wire x1="-9.75" y1="-8.8" x2="9.75" y2="-8.8" width="0.127" layer="39"/>
<wire x1="-9.75" y1="8.8" x2="-9.75" y2="-8.8" width="0.127" layer="39"/>
<wire x1="9.7" y1="8.8" x2="9.75" y2="-8.8" width="0.127" layer="39"/>
<wire x1="8.55" y1="6.05" x2="8.55" y2="2.05" width="0.127" layer="21"/>
<wire x1="8.55" y1="-6.05" x2="8.55" y2="-2.15" width="0.127" layer="21"/>
<text x="-4.4" y="10.4" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.4" y="-11.6" size="1.27" layer="27">&gt;VALUE</text>
<text x="10.2" y="-0.6" size="1.27" layer="21">+</text>
</package>
</packages>
<packages3d>
<package3d name="2.6X1.6X1(BXCXA)" urn="urn:adsk.eagle:package:7562384/1" type="box">
<packageinstances>
<packageinstance name="2.6X1.6X1(BXCXA)"/>
</packageinstances>
</package3d>
<package3d name="3X1.6X1.4(BXCXA)" urn="urn:adsk.eagle:package:7562383/1" type="box">
<packageinstances>
<packageinstance name="3X1.6X1.4(BXCXA)"/>
</packageinstances>
</package3d>
<package3d name="3.5X1.6X2.1(BXCXA)" urn="urn:adsk.eagle:package:7562382/1" type="box">
<packageinstances>
<packageinstance name="3.5X1.6X2.1(BXCXA)"/>
</packageinstances>
</package3d>
<package3d name="4.2X1.9X2.8(BXCXA)" urn="urn:adsk.eagle:package:7562380/1" type="box">
<packageinstances>
<packageinstance name="4.2X1.9X2.8(BXCXA)"/>
</packageinstances>
</package3d>
<package3d name="4.4X1.9X4.3(BXCXA)" urn="urn:adsk.eagle:package:7562381/1" type="box">
<packageinstances>
<packageinstance name="4.4X1.9X4.3(BXCXA)"/>
</packageinstances>
</package3d>
<package3d name="5.8X2.5X4.3(BXCXA)" urn="urn:adsk.eagle:package:7562379/1" type="box">
<packageinstances>
<packageinstance name="5.8X2.5X4.3(BXCXA)"/>
</packageinstances>
</package3d>
<package3d name="6.5X3.5X6(BXCXA)" urn="urn:adsk.eagle:package:7562378/1" type="box">
<packageinstances>
<packageinstance name="6.5X3.5X6(BXCXA)"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="C_POLARIZED" urn="urn:adsk.eagle:symbol:7562208/1" library_version="1">
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<text x="3.175" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="3.175" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.127" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="1.905" width="0.127" layer="94"/>
<wire x1="-2.54" y1="1.905" x2="2.54" y2="1.905" width="0.127" layer="94"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="2.54" width="0.127" layer="94"/>
<wire x1="-1.905" y1="3.175" x2="-1.905" y2="4.445" width="0.127" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-1.27" y2="3.81" width="0.127" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-1.27" y2="-1.27" width="0.127" layer="94"/>
<rectangle x1="-0.3175" y1="-2.2225" x2="0.3175" y2="2.8575" layer="94" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WCAP-ASLI" urn="urn:adsk.eagle:component:7562552/1" prefix="C" uservalue="yes" library_version="1">
<description>&lt;b&gt;WCAP-ASLI Aluminum Electrolytic Capacitors
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;Characteristics
&lt;br&gt;&lt;/b&gt;Aluminum Electrolytic Capacitor
&lt;br&gt;Recommended soldering: Reflow
&lt;br&gt;Operating temperature: -55 °C to +105 °C
&lt;br&gt;Low impedance product series
&lt;br&gt;Load life:2000 h @ +105 °C
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;&lt;b&gt; Package name and Variant abbreviation:
&lt;br&gt;&lt;/b&gt; Package:
&lt;br&gt;Package: B X C X A = length of the solder pad X width of the solder pad X pitch of the solder pad.

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;&lt;a href="http://katalog.we-online.de/media/images/eisos/WCAP-ASLI_pf2.jpg" title="Enlarge picture"&gt;
&lt;img src="http://katalog.we-online.de/media/thumbs2/eisos/thb_WCAP-ASLI_pf2.jpg" width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="http://katalog.we-online.de/en/pbs/WCAP-ASLI"&gt;http://katalog.we-online.de/en/pbs/WCAP-ASLI
&lt;/a&gt;&lt;p&gt;Updated by: Yingchun,Shan
&lt;/a&gt;&lt;p&gt;Updated: 2017-03-16
&lt;br&gt;2017 (C) Wurth Elektronik</description>
<gates>
<gate name="G$1" symbol="C_POLARIZED" x="0" y="0"/>
</gates>
<devices>
<device name="_865080140001" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080140001/22uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080142005" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080142005/56uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080143008" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080143008/150uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080153012" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080153012/470uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080157015" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080157016/1200uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080162017" package="5.8X2.5X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562379/1"/>
</package3dinstances>
<technologies>
<technology name="_12,5X14">
<attribute name="WE-NUMBER" value="865080162017/3300uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080163018" package="6.5X3.5X6(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562378/1"/>
</package3dinstances>
<technologies>
<technology name="_16X17">
<attribute name="WE-NUMBER" value="865080163018/6800uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080145010" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080145010/330uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080149011" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X6,5">
<attribute name="WE-NUMBER" value="865080149011/330uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080864003" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X6,1">
<attribute name="WE-NUMBER" value="865080864003/3,3uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080140002" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080140002/27uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080140003" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080140003/33uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080140004" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080140004/47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080240001" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080240001/22uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080240003" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080240003/33uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080340001" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080340001/10uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080340002" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080340002/15uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080340003" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080340003/22uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080440001" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080440001/6,8uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080440002" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080540001/3,3uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080540002" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080540002/4,7uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080540003" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080540003/6,8uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080540004" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080540004/10uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080640001" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080640001/1uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080640002" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080640002/2,2uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080640003" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080640003/3,3uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080640004" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080640004/4,7uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080740002" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080740002/0,1uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080142006" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080142006/68uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080142007" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080142007/100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080242002" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080242002/27uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080242004" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080242004/47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080342004" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080342004/27uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080342006" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080342006/47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080442003" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080442003/15uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080442004" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080442004/22uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080442006" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080442006/33uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080542005" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080542005/15uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080542006" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5"/>
</technologies>
</device>
<device name="_865080642005" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080642005/6,8uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080642006" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080642006/10uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080143009" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080143009/220uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080243005" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080243005/56uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080243006" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080243006/68uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080243007" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080243007/100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080243008" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5"/>
</technologies>
</device>
<device name="_865080343005" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080343005/33uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080343007" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080343007/56uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080343008" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080343008/68uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080343009" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080343009/100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080443005" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080443005/27uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080443007" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080443007/47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080443008" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080443008/56uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080443009" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080443009/68uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080543007" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080543007/27uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080543008" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080543008/33uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080543009" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080543009/47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080643007" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080643007/15uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080643008" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080643008/22uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080245009" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080245009/220uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080345010" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080345010/150uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080345012" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080345012/220uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080445010" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080445010/100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080545010" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080545010/56uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080545011" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080545011/68uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080545012" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080545012/100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080645009" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080645009/27uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080645010" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080645010/33uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080645012" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080645012/47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080153013" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080153013/680uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080153014" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080153014/1000uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080253011" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080253011/330uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080253012" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080253012/470uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080353014" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="W-NUMBER" value="865080353014/330uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080353015" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080353015/470uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080453012" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080453012/150uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080453013" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080453013/220uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080453014" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080453014/330uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080553013" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080553013/150uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080553014" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080553014/220uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080653014" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080653014/56uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080653015" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080653015/68uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080653016" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080653016/100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080249010" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X6,5">
<attribute name="WE-NUMBER" value="865080249010/220uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080349011" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X6,5">
<attribute name="WE-NUMBER" value="865080349011/150uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080349013" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X6,5">
<attribute name="WE-NUMBER" value="865080349013/220uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080449011" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X6,5">
<attribute name="WE-NUMBER" value="865080449011/100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080649011" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X6,5">
<attribute name="WE-NUMBER" value="865080649011/33uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080649013" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X6,5">
<attribute name="WE-NUMBER" value="865080649013/47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080157016" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080157016/1500uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080257013" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080257013/680uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080257014" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080257014/100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080357016" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080357016/680uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080457015" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080457015/470uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080557015" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080557015/330uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080657017" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080657017/150uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080657018" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080657018/220uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080262015" package="5.8X2.5X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562379/1"/>
</package3dinstances>
<technologies>
<technology name="_12,5X14">
<attribute name="WE-NUMBER" value="865080262015/2200uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080362017" package="5.8X2.5X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562379/1"/>
</package3dinstances>
<technologies>
<technology name="_12,5X14">
<attribute name="WE-NUMBER" value="865080362017/1500uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080462016" package="5.8X2.5X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562379/1"/>
</package3dinstances>
<technologies>
<technology name="_12,5X14">
<attribute name="WE-NUMBER" value="865080462016/1000uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080562016" package="5.8X2.5X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562379/1"/>
</package3dinstances>
<technologies>
<technology name="_12,5X14">
<attribute name="WE-NUMBER" value="865080562016/470uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080562017" package="5.8X2.5X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562379/1"/>
</package3dinstances>
<technologies>
<technology name="_12,5X14">
<attribute name="WE-NUMBER" value="865080562017/680uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080662019" package="5.8X2.5X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562379/1"/>
</package3dinstances>
<technologies>
<technology name="_12,5X14">
<attribute name="WE-NUMBER" value="865080662019/330uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080263016" package="6.5X3.5X6(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562378/1"/>
</package3dinstances>
<technologies>
<technology name="_16X17">
<attribute name="WE-NUMBER" value="865080263016/4700uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080363018" package="6.5X3.5X6(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562378/1"/>
</package3dinstances>
<technologies>
<technology name="_16X17">
<attribute name="WE-NUMBER" value="865080363018/3300uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080463017" package="6.5X3.5X6(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562378/1"/>
</package3dinstances>
<technologies>
<technology name="_16X17">
<attribute name="WE-NUMBER" value="865080463017/2200uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080563018" package="6.5X3.5X6(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562378/1"/>
</package3dinstances>
<technologies>
<technology name="_16X17">
<attribute name="WE-NUMBER" value="865080563018/1500uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080663020" package="6.5X3.5X6(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562378/1"/>
</package3dinstances>
<technologies>
<technology name="_16X17">
<attribute name="WE-NUMBER" value="865080663020/1000uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080763016" package="6.5X3.5X6(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562378/1"/>
</package3dinstances>
<technologies>
<technology name="_16X17">
<attribute name="WE-NUMBER" value="865080763016/470uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080863009" package="6.5X3.5X6(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562378/1"/>
</package3dinstances>
<technologies>
<technology name="_16X17">
<attribute name="WE-NUMBER" value="865080863009/150uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865081763010" package="6.5X3.5X6(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562378/1"/>
</package3dinstances>
<technologies>
<technology name="_16X17">
<attribute name="WE-NUMBER" value="865081763010/150uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865081763011" package="6.5X3.5X6(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562378/1"/>
</package3dinstances>
<technologies>
<technology name="_16X17">
<attribute name="WE-NUMBER" value="865081763011/220uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080540001" package="2.6X1.6X1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562384/1"/>
</package3dinstances>
<technologies>
<technology name="_4X5,5">
<attribute name="WE-NUMBER" value="865080540001/3,3uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080742008" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080742008/3,3uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080742009" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865080742009/4,7uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865081742002" package="3X1.6X1.4(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562383/1"/>
</package3dinstances>
<technologies>
<technology name="_5X5,5">
<attribute name="WE-NUMBER" value="865081742002/2,2uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080753012" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080753012/33uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080753013" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080753013/47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080853006" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865080853006/22uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865081753007" package="4.2X1.9X2.8(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562380/1"/>
</package3dinstances>
<technologies>
<technology name="_8X10,5">
<attribute name="WE-NUMBER" value="865081753007/33uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080757014" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080757014/100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080857007" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865080857007/33uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865081757008" package="4.4X1.9X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562381/1"/>
</package3dinstances>
<technologies>
<technology name="_10X10,5">
<attribute name="WE-NUMBER" value="865081757008/47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080762015" package="5.8X2.5X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562379/1"/>
</package3dinstances>
<technologies>
<technology name="_12,5X14">
<attribute name="WE-NUMBER" value="865080762015/220uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080862008" package="5.8X2.5X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562379/1"/>
</package3dinstances>
<technologies>
<technology name="_12,5X14">
<attribute name="WE-NUMBER" value="865080862008/47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865081762009" package="5.8X2.5X4.3(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562379/1"/>
</package3dinstances>
<technologies>
<technology name="_12,5X14">
<attribute name="WE-NUMBER" value="865081762009/100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080743010" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080743010/10uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080843002" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865080843002/2,2uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865081743003" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865081743003/3,3uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865081743004" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X5,5">
<attribute name="WE-NUMBER" value="865081743004/4,7uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080745011" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080745011/22uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080845004" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080845004/4,7uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865080845005" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865080845005/10uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865081745005" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865081745005/10uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_865081745006" package="3.5X1.6X2.1(BXCXA)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7562382/1"/>
</package3dinstances>
<technologies>
<technology name="_6,3X7,7">
<attribute name="WE-NUMBER" value="865081745006/22uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BeagleBone_Blue">
<description>Generated from &lt;b&gt;BeagleBone_Blue.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
<package name="SPARKFUN-RESISTORS_0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="SPARKFUN-RESISTORS_0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SPARKFUN-RESISTORS_1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SPARKFUN-RESISTORS_R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SPARKFUN-RESISTORS_R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SPARKFUN-RESISTORS_AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="SPARKFUN-RESISTORS_AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="SPARKFUN-RESISTORS_AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="SPARKFUN-RESISTORS_AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="SPARKFUN-RESISTORS_AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="SPARKFUN-RESISTORS_1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762" diameter="1.6764"/>
<pad name="2" x="2.5" y="0" drill="0.762" diameter="1.6764"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="SPARKFUN-RESISTORS_AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="SPARKFUN-RESISTORS_AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="SPARKFUN-RESISTORS_RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPARKFUN-RESISTORS_RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="SPARKFUN-RESISTORS_RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="SPARKFUN-RESISTORS_0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="SPARKFUN-RESISTORS_0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="SPARKFUN-RESISTORS_0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="SPARKFUN-RESISTORS_1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="SPARKFUN-RESISTORS_R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="SPARKFUN-RESISTORS_R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="SPARKFUN-RESISTORS_AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="SPARKFUN-RESISTORS_AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="SPARKFUN-RESISTORS_AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="SPARKFUN-RESISTORS_AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="SPARKFUN-RESISTORS_AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="SPARKFUN-RESISTORS_1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="SPARKFUN-RESISTORS_AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="SPARKFUN-RESISTORS_AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="B05B-PASK_LF__SN_">
<description>&lt;B05B-PASK(LF)(SN)&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="B05BPASKLFSN">
<description>&lt;b&gt;B05B-PASK(LF)(SN)-2&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="0.91" diameter="1.416"/>
<pad name="2" x="2" y="0" drill="0.91" diameter="1.416"/>
<pad name="3" x="4" y="0" drill="0.91" diameter="1.416"/>
<pad name="4" x="6" y="0" drill="0.91" diameter="1.416"/>
<pad name="5" x="8" y="0" drill="0.91" diameter="1.416"/>
<text x="4" y="0.575" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="4" y="0.575" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2" y1="-2.65" x2="10" y2="-2.65" width="0.2" layer="51"/>
<wire x1="10" y1="-2.65" x2="10" y2="2.65" width="0.2" layer="51"/>
<wire x1="10" y1="2.65" x2="-2" y2="2.65" width="0.2" layer="51"/>
<wire x1="-2" y1="2.65" x2="-2" y2="-2.65" width="0.2" layer="51"/>
<wire x1="-2" y1="2.65" x2="10" y2="2.65" width="0.1" layer="21"/>
<wire x1="10" y1="2.65" x2="10" y2="-2.65" width="0.1" layer="21"/>
<wire x1="10" y1="-2.65" x2="-2" y2="-2.65" width="0.1" layer="21"/>
<wire x1="-2" y1="-2.65" x2="-2" y2="2.65" width="0.1" layer="21"/>
<wire x1="-3" y1="4.8" x2="11" y2="4.8" width="0.1" layer="51"/>
<wire x1="11" y1="4.8" x2="11" y2="-3.65" width="0.1" layer="51"/>
<wire x1="11" y1="-3.65" x2="-3" y2="-3.65" width="0.1" layer="51"/>
<wire x1="-3" y1="-3.65" x2="-3" y2="4.8" width="0.1" layer="51"/>
<wire x1="-0.1" y1="3.75" x2="-0.1" y2="3.75" width="0.2" layer="21"/>
<wire x1="-0.1" y1="3.75" x2="0" y2="3.75" width="0.2" layer="21" curve="180"/>
<wire x1="0" y1="3.75" x2="0" y2="3.75" width="0.2" layer="21"/>
<wire x1="0" y1="3.75" x2="-0.1" y2="3.75" width="0.2" layer="21" curve="180"/>
<wire x1="-0.1" y1="3.75" x2="-0.1" y2="3.75" width="0.2" layer="21"/>
<wire x1="-0.1" y1="3.75" x2="0" y2="3.75" width="0.2" layer="21" curve="180"/>
</package>
</packages>
<symbols>
<symbol name="B05B-PASK_LF__SN_">
<wire x1="5.08" y1="2.54" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<text x="16.51" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" length="middle"/>
<pin name="2" x="0" y="-2.54" length="middle"/>
<pin name="3" x="0" y="-5.08" length="middle"/>
<pin name="4" x="0" y="-7.62" length="middle"/>
<pin name="5" x="0" y="-10.16" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="B05B-PASK_LF__SN_" prefix="J">
<description>&lt;b&gt;B05B-PASK(LF)(SN)&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.farnell.com/datasheets/1759149.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="B05B-PASK_LF__SN_" x="0" y="0"/>
</gates>
<devices>
<device name="" package="B05BPASKLFSN">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="B05B-PASK(LF)(SN)" constant="no"/>
<attribute name="HEIGHT" value="7.7mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="JST (JAPAN SOLDERLESS TERMINALS)" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="B05B-PASK(LF)(SN)" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC" urn="urn:adsk.eagle:symbol:13874/1" library_version="1">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" urn="urn:adsk.eagle:component:13926/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fuses">
<packages>
<package name="FUSE-HOLDER-0031.7701.11">
<smd name="P$1" x="-4" y="0" dx="5" dy="6" layer="1"/>
<smd name="P$2" x="4" y="0" dx="5" dy="6" layer="1"/>
<wire x1="6" y1="2.6" x2="6" y2="-2.6" width="0.127" layer="21"/>
<wire x1="6" y1="-2.6" x2="-6" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-6" y1="-2.6" x2="-6" y2="2.6" width="0.127" layer="21"/>
<wire x1="-6" y1="2.6" x2="6" y2="2.6" width="0.127" layer="21"/>
<rectangle x1="-3.81" y1="-1.27" x2="3.81" y2="1.27" layer="21"/>
<text x="0" y="0" size="1.27" layer="21">&gt;Name</text>
<text x="0" y="0" size="1.27" layer="21">&gt;Name</text>
<text x="0" y="0" size="1.27" layer="21">&gt;Name</text>
<text x="0" y="0" size="1.27" layer="21">&gt;Name</text>
<text x="0" y="1.27" size="0.6096" layer="25" rot="R90">&gt;Name</text>
</package>
<package name="FUSE-OMF-63">
<text x="0" y="0" size="0.508" layer="21">&gt;Name</text>
</package>
</packages>
<symbols>
<symbol name="FUSE-HOLDER">
<wire x1="-5.08" y1="0" x2="-3.556" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.889" y1="-1.4986" x2="2.4892" y2="0" width="0.254" layer="94"/>
<wire x1="-3.5992" y1="1.4912" x2="-3.048" y2="1.7272" width="0.254" layer="94" curve="-46.337037" cap="flat"/>
<wire x1="-3.048" y1="1.7272" x2="-2.496" y2="1.491" width="0.254" layer="94" curve="-46.403624" cap="flat"/>
<wire x1="0.4572" y1="-1.778" x2="0.8965" y2="-1.4765" width="0.254" layer="94" curve="63.169357" cap="flat"/>
<wire x1="-0.0178" y1="-1.508" x2="0.4572" y2="-1.7778" width="0.254" layer="94" curve="64.986119" cap="flat"/>
<text x="-5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas" function="dot" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" function="dot" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="FUSE-CARTRIDGE">
<wire x1="-5.08" y1="0" x2="-3.556" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.889" y1="-1.4986" x2="2.4892" y2="0" width="0.254" layer="94"/>
<wire x1="-3.5992" y1="1.4912" x2="-3.048" y2="1.7272" width="0.254" layer="94" curve="-46.337037" cap="flat"/>
<wire x1="-3.048" y1="1.7272" x2="-2.496" y2="1.491" width="0.254" layer="94" curve="-46.403624" cap="flat"/>
<wire x1="0.4572" y1="-1.778" x2="0.8965" y2="-1.4765" width="0.254" layer="94" curve="63.169357" cap="flat"/>
<wire x1="-0.0178" y1="-1.508" x2="0.4572" y2="-1.7778" width="0.254" layer="94" curve="64.986119" cap="flat"/>
<text x="-5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FUSE-HOLDER-0031.7701.11" prefix="F">
<gates>
<gate name="G$1" symbol="FUSE-HOLDER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FUSE-HOLDER-0031.7701.11">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE-OMF-63" prefix="F'">
<gates>
<gate name="G$1" symbol="FUSE-CARTRIDGE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FUSE-OMF-63">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="PMEG3050EP_115">
<description>&lt;NXP PMEG3050EP,115 SMT Schottky Diode, 30V 5A, 2-Pin SOD-128&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="PMEG3050EP115">
<description>&lt;b&gt;SOD128&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.2" y="0" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="2.2" y="0" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<text x="-9.19" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="9.86" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.9" y1="1.25" x2="1.9" y2="1.25" width="0.2" layer="51"/>
<wire x1="1.9" y1="1.25" x2="1.9" y2="-1.25" width="0.2" layer="51"/>
<wire x1="1.9" y1="-1.25" x2="-1.9" y2="-1.25" width="0.2" layer="51"/>
<wire x1="-1.9" y1="-1.25" x2="-1.9" y2="1.25" width="0.2" layer="51"/>
<wire x1="-4.5" y1="2.25" x2="3.9" y2="2.25" width="0.1" layer="51"/>
<wire x1="3.9" y1="2.25" x2="3.9" y2="-2.25" width="0.1" layer="51"/>
<wire x1="3.9" y1="-2.25" x2="-4.5" y2="-2.25" width="0.1" layer="51"/>
<wire x1="-4.5" y1="-2.25" x2="-4.5" y2="2.25" width="0.1" layer="51"/>
<wire x1="-1.9" y1="-1.25" x2="1.9" y2="-1.25" width="0.1" layer="21"/>
<wire x1="-1.9" y1="1.25" x2="1.9" y2="1.25" width="0.1" layer="21"/>
<wire x1="-3.4" y1="0.1" x2="-3.4" y2="-0.1" width="0.2" layer="21" curve="-180"/>
<wire x1="-3.4" y1="-0.1" x2="-3.4" y2="0.1" width="0.2" layer="21" curve="-180"/>
</package>
</packages>
<symbols>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PMEG3050EP,115" prefix="D">
<description>&lt;b&gt;NXP PMEG3050EP,115 SMT Schottky Diode, 30V 5A, 2-Pin SOD-128&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://docs-emea.rs-online.com/webdocs/14cd/0900766b814cd012.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PMEG3050EP115">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="NXP PMEG3050EP,115 SMT Schottky Diode, 30V 5A, 2-Pin SOD-128" constant="no"/>
<attribute name="HEIGHT" value="1mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Nexperia" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="PMEG3050EP,115" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="771-PMEG3050EP115" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=771-PMEG3050EP115" constant="no"/>
<attribute name="RS_PART_NUMBER" value="7805398P" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="http://uk.rs-online.com/web/p/products/7805398P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.3048" drill="0.508">
<clearance class="0" value="0.254"/>
</class>
</classes>
<parts>
<part name="TENSY_4.1" library="Tensy_4.0" deviceset="TENSY_4.0" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J1" library="292133-3" deviceset="292133-3" device="">
<attribute name="DIGIKEY" value="455-1833-ND"/>
<attribute name="MPN" value="B03B-PASK-1(LF)(SN)"/>
</part>
<part name="J2" library="292133-3" deviceset="292133-3" device="">
<attribute name="DIGIKEY" value="455-1833-ND"/>
<attribute name="MPN" value="B03B-PASK-1(LF)(SN)"/>
</part>
<part name="GND2" library="PulseOximeterECG_AU" library_urn="urn:adsk.eagle:library:14846661" deviceset="GND" device=""/>
<part name="GND3" library="PulseOximeterECG_AU" library_urn="urn:adsk.eagle:library:14846661" deviceset="GND" device=""/>
<part name="J4" library="SparkFun-Connectors" deviceset="CONN_03" device="-SCREW-5MM"/>
<part name="J5" library="SparkFun-Connectors" deviceset="CONN_02" device="3.5MM"/>
<part name="J6" library="SparkFun-Connectors" deviceset="CONN_02" device="3.5MM"/>
<part name="GND1" library="PulseOximeterECG_AU" library_urn="urn:adsk.eagle:library:14846661" deviceset="GND" device=""/>
<part name="U1" library="DRV8800PWPR" deviceset="DRV8800PWPR" device="">
<attribute name="DIGIKEY" value="296-25377-1-ND"/>
</part>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C5" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1 uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="C6" library="Wurth_Elektronik_Passive_Capacitors_rev18b" library_urn="urn:adsk.eagle:library:7562178" deviceset="WCAP-ASLI" device="_865080343009" package3d_urn="urn:adsk.eagle:package:7562382/1" technology="_6,3X5,5" value="100 uF">
<attribute name="DIGIKEY" value="732-8419-1-ND"/>
<attribute name="MF" value="Würth Elektronik"/>
<attribute name="MPN" value="865080343009"/>
</part>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C7" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1 uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="C8" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1 uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="R3" library="BeagleBone_Blue" deviceset="SPARKFUN-RESISTORS_RESISTOR" device="0805-RES" value="10k">
<attribute name="DIGIKEY" value="541-10KACT-ND"/>
<attribute name="MF" value="Vishay Dale"/>
<attribute name="MPN" value="CRCW080510K0JNEA"/>
</part>
<part name="R4" library="BeagleBone_Blue" deviceset="SPARKFUN-RESISTORS_RESISTOR" device="0805-RES" value="0.2">
<attribute name="DIGIKEY" value="73L3R20JCT-ND"/>
<attribute name="MF" value="CTS Resistor Products"/>
<attribute name="MPN" value="73L3R20J"/>
</part>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U2" library="DRV8800PWPR" deviceset="DRV8800PWPR" device="">
<attribute name="DIGIKEY" value="296-25377-1-ND"/>
<attribute name="MPN" value="DRV8800PWPR"/>
</part>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C1" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1µF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="C2" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1µF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="C3" library="Wurth_Elektronik_Passive_Capacitors_rev18b" library_urn="urn:adsk.eagle:library:7562178" deviceset="WCAP-ASLI" device="_865080343009" package3d_urn="urn:adsk.eagle:package:7562382/1" technology="_6,3X5,5" value="100µF">
<attribute name="DIGIKEY" value="732-8419-1-ND"/>
<attribute name="MF" value="Würth Elektronik"/>
<attribute name="MPN" value="865080343009"/>
</part>
<part name="C4" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1µF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R1" library="BeagleBone_Blue" deviceset="SPARKFUN-RESISTORS_RESISTOR" device="0805-RES" value="10k">
<attribute name="DIGIKEY" value="541-10KACT-ND"/>
<attribute name="MF" value="Vishay Dale"/>
<attribute name="MPN" value="CRCW080510K0JNEA"/>
</part>
<part name="R2" library="BeagleBone_Blue" deviceset="SPARKFUN-RESISTORS_RESISTOR" device="0805-RES" value="0.2">
<attribute name="DIGIKEY" value="73L3R20JCT-ND"/>
<attribute name="MF" value="CTS Resistor Products"/>
<attribute name="MPN" value="73L3R20J"/>
</part>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C9" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.22 uF">
<attribute name="DIGIKEY" value="1276-6478-1-ND"/>
<attribute name="MF" value="Samsung Electro-Mechanics"/>
<attribute name="MPN" value="CL21B224KBFNNNG"/>
</part>
<part name="C10" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.22µF">
<attribute name="DIGIKEY" value="1276-6478-1-ND"/>
<attribute name="MF" value="Samsung Electro-Mechanics"/>
<attribute name="MPN" value="CL21B224KBFNNNG"/>
</part>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J7" library="B05B-PASK_LF__SN_" deviceset="B05B-PASK_LF__SN_" device="">
<attribute name="DIGIKEY" value="455-1821-ND"/>
<attribute name="MF" value="JST Sales America Inc."/>
<attribute name="MPN" value="B06B-PASK(LF)(SN)"/>
</part>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J8" library="B05B-PASK_LF__SN_" deviceset="B05B-PASK_LF__SN_" device="">
<attribute name="DIGIKEY" value="455-1821-ND"/>
<attribute name="MF" value="JST Sales America Inc."/>
<attribute name="MPN" value="B05B-PASK(LF)(SN)"/>
</part>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J9" library="B05B-PASK_LF__SN_" deviceset="B05B-PASK_LF__SN_" device="">
<attribute name="DIGIKEY" value="455-1821-ND"/>
<attribute name="MF" value="JST Sales America Inc."/>
<attribute name="MPN" value="B06B-PASK(LF)(SN)"/>
</part>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J10" library="B05B-PASK_LF__SN_" deviceset="B05B-PASK_LF__SN_" device="">
<attribute name="DIGIKEY" value="455-1821-ND"/>
<attribute name="MF" value="JST Sales America Inc."/>
<attribute name="MPN" value="B06B-PASK(LF)(SN)"/>
</part>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J11" library="B05B-PASK_LF__SN_" deviceset="B05B-PASK_LF__SN_" device="">
<attribute name="DIGIKEY" value="455-1821-ND"/>
<attribute name="MF" value="JST Sales America Inc."/>
<attribute name="MPN" value="B05B-PASK(LF)(SN)"/>
</part>
<part name="GND21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J12" library="B05B-PASK_LF__SN_" deviceset="B05B-PASK_LF__SN_" device="">
<attribute name="DIGIKEY" value="455-1821-ND"/>
<attribute name="MF" value="JST Sales America Inc."/>
<attribute name="MPN" value="B05B-PASK(LF)(SN)"/>
</part>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="FRAME2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="FRAME3" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="FRAME6" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="FRAME4" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="GND4" library="PulseOximeterECG_AU" library_urn="urn:adsk.eagle:library:14846661" deviceset="GND" device=""/>
<part name="J3" library="292133-3" deviceset="292133-3" device="">
<attribute name="DIGIKEY" value="455-1833-ND"/>
<attribute name="MPN" value="B03B-PASK-1(LF)(SN)"/>
</part>
<part name="F1" library="fuses" deviceset="FUSE-HOLDER-0031.7701.11" device="">
<attribute name="DIGIKEY" value="486-1991-ND"/>
<attribute name="MF" value="Schurter Inc."/>
<attribute name="MPN" value="0031.7701.11"/>
</part>
<part name="F'1" library="fuses" deviceset="FUSE-OMF-63" device="" value="4A">
<attribute name="DIGIKEY" value="486-3935-1-ND"/>
<attribute name="MF" value="Schurter Inc."/>
<attribute name="MPN" value="3402.0016.22"/>
</part>
<part name="D1" library="PMEG3050EP_115" deviceset="PMEG3050EP,115" device=""/>
<part name="J13" library="SparkFun-Connectors" deviceset="CONN_02" device="3.5MM"/>
</parts>
<sheets>
<sheet>
<description>Teensy 4.0</description>
<plain>
<text x="93.98" y="134.62" size="6.4516" layer="97">Teensy 4.0</text>
</plain>
<instances>
<instance part="TENSY_4.1" gate="G$1" x="106.68" y="81.28" smashed="yes"/>
<instance part="GND7" gate="1" x="99.06" y="119.38" smashed="yes" rot="R180">
<attribute name="VALUE" x="101.6" y="121.92" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND10" gate="1" x="132.08" y="116.84" smashed="yes" rot="R180">
<attribute name="VALUE" x="134.62" y="119.38" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="FRAME3" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="J13" gate="G$1" x="195.58" y="88.9" smashed="yes" rot="MR0">
<attribute name="VALUE" x="198.12" y="84.074" size="1.778" layer="96" font="vector" rot="MR0"/>
<attribute name="NAME" x="198.12" y="94.488" size="1.778" layer="95" font="vector" rot="MR0"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="SERVO1_PMW" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="0"/>
<wire x1="104.14" y1="114.3" x2="99.06" y2="114.3" width="0.1524" layer="91"/>
<label x="99.06" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="GND"/>
<wire x1="104.14" y1="116.84" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="GND_3"/>
<wire x1="127" y1="114.3" x2="132.08" y2="114.3" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
</net>
<net name="SERVO3_PMW" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="1"/>
<wire x1="104.14" y1="111.76" x2="99.06" y2="111.76" width="0.1524" layer="91"/>
<label x="99.06" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SERVO2_PMW" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="2"/>
<wire x1="104.14" y1="109.22" x2="99.06" y2="109.22" width="0.1524" layer="91"/>
<label x="99.06" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NFAULT_GPIO_U1" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="22"/>
<wire x1="127" y1="106.68" x2="132.08" y2="106.68" width="0.1524" layer="91"/>
<label x="132.08" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MODE_GPIO_U1" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="21"/>
<wire x1="127" y1="104.14" x2="132.08" y2="104.14" width="0.1524" layer="91"/>
<label x="132.08" y="104.14" size="1.27" layer="95" xref="yes"/>
<label x="132.08" y="104.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PHASE_GPIO_U1" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="20"/>
<wire x1="127" y1="101.6" x2="132.08" y2="101.6" width="0.1524" layer="91"/>
<label x="132.08" y="101.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ENABLE_GPIO_U1" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="16"/>
<wire x1="127" y1="91.44" x2="132.08" y2="91.44" width="0.1524" layer="91"/>
<label x="132.08" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VMCU_3.3V" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="3.3V"/>
<wire x1="127" y1="111.76" x2="132.08" y2="111.76" width="0.1524" layer="91"/>
<label x="132.08" y="111.76" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="I2C_SCL0" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="19"/>
<wire x1="127" y1="99.06" x2="132.08" y2="99.06" width="0.1524" layer="91"/>
<label x="132.08" y="99.06" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="I2C_SDA0" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="18"/>
<wire x1="127" y1="96.52" x2="132.08" y2="96.52" width="0.1524" layer="91"/>
<label x="132.08" y="96.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NSLEEP_GPIO_U1" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="17"/>
<wire x1="127" y1="93.98" x2="132.08" y2="93.98" width="0.1524" layer="91"/>
<label x="132.08" y="93.98" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="XSHUT1" class="0">
<segment>
<wire x1="104.14" y1="104.14" x2="99.06" y2="104.14" width="0.1524" layer="91"/>
<label x="99.06" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="4"/>
</segment>
</net>
<net name="XSHUT2" class="0">
<segment>
<wire x1="104.14" y1="101.6" x2="99.06" y2="101.6" width="0.1524" layer="91"/>
<label x="99.06" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="5"/>
</segment>
</net>
<net name="XSHUT3" class="0">
<segment>
<wire x1="104.14" y1="99.06" x2="99.06" y2="99.06" width="0.1524" layer="91"/>
<label x="99.06" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="6"/>
</segment>
</net>
<net name="NSLEEP_GPIO_U2" class="0">
<segment>
<wire x1="104.14" y1="83.82" x2="99.06" y2="83.82" width="0.1524" layer="91"/>
<label x="99.06" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="12"/>
</segment>
</net>
<net name="XSHUT4" class="0">
<segment>
<wire x1="104.14" y1="96.52" x2="99.06" y2="96.52" width="0.1524" layer="91"/>
<label x="99.06" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="7"/>
</segment>
</net>
<net name="ENABLE_GPIO_U2" class="0">
<segment>
<wire x1="104.14" y1="86.36" x2="99.06" y2="86.36" width="0.1524" layer="91"/>
<label x="99.06" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="11"/>
</segment>
</net>
<net name="XSHUT5" class="0">
<segment>
<wire x1="104.14" y1="93.98" x2="99.06" y2="93.98" width="0.1524" layer="91"/>
<label x="99.06" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="8"/>
</segment>
</net>
<net name="XSHUT6" class="0">
<segment>
<wire x1="104.14" y1="88.9" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<label x="99.06" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="10"/>
</segment>
</net>
<net name="NFAULT_GPIO_U2" class="0">
<segment>
<wire x1="127" y1="88.9" x2="132.08" y2="88.9" width="0.1524" layer="91"/>
<label x="132.08" y="88.9" size="1.27" layer="95" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="15"/>
</segment>
</net>
<net name="MODE_GPIO_U2" class="0">
<segment>
<wire x1="127" y1="86.36" x2="132.08" y2="86.36" width="0.1524" layer="91"/>
<label x="132.08" y="86.36" size="1.27" layer="95" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="14"/>
</segment>
</net>
<net name="PHASE_GPIO_U2" class="0">
<segment>
<wire x1="132.08" y1="83.82" x2="127" y2="83.82" width="0.1524" layer="91"/>
<label x="132.08" y="83.82" size="1.27" layer="95" xref="yes"/>
<pinref part="TENSY_4.1" gate="G$1" pin="13"/>
</segment>
</net>
<net name="ON/OFF" class="0">
<segment>
<pinref part="TENSY_4.1" gate="G$1" pin="ON/OFF"/>
<wire x1="104.14" y1="81.28" x2="99.06" y2="81.28" width="0.1524" layer="91"/>
<label x="99.06" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="187.96" y1="91.44" x2="182.88" y2="91.44" width="0.1524" layer="91"/>
<label x="182.88" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J13" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="162.56" y="124.46" size="6.4516" layer="97">Servos</text>
<text x="205.74" y="73.66" size="6.4516" layer="97">WAGO</text>
</plain>
<instances>
<instance part="J1" gate="G$1" x="193.04" y="149.86" smashed="yes" rot="MR180">
<attribute name="NAME" x="191.27" y="144.78" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="187.96" y="157.48" size="1.778" layer="96" rot="MR180"/>
<attribute name="DIGIKEY" x="193.04" y="149.86" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="MPN" x="193.04" y="149.86" size="1.778" layer="96" rot="MR180" display="off"/>
</instance>
<instance part="J2" gate="G$1" x="132.08" y="149.86" smashed="yes" rot="MR180">
<attribute name="NAME" x="131.58" y="144.78" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="127" y="157.48" size="1.778" layer="96" rot="MR180"/>
<attribute name="DIGIKEY" x="132.08" y="149.86" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="MPN" x="132.08" y="149.86" size="1.778" layer="96" rot="MR180" display="off"/>
</instance>
<instance part="GND2" gate="1" x="115.57" y="144.78" smashed="yes">
<attribute name="VALUE" x="113.03" y="142.24" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="180.34" y="144.78" smashed="yes">
<attribute name="VALUE" x="177.8" y="142.24" size="1.778" layer="96"/>
</instance>
<instance part="J4" gate="J$1" x="27.94" y="101.6" smashed="yes">
<attribute name="VALUE" x="25.4" y="94.234" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="25.4" y="107.188" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="J5" gate="G$1" x="185.42" y="81.28" smashed="yes" rot="MR0">
<attribute name="VALUE" x="187.96" y="76.454" size="1.778" layer="96" font="vector" rot="MR0"/>
<attribute name="NAME" x="187.96" y="86.868" size="1.778" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="J6" gate="G$1" x="185.42" y="63.5" smashed="yes" rot="MR0">
<attribute name="VALUE" x="187.96" y="58.674" size="1.778" layer="96" font="vector" rot="MR0"/>
<attribute name="NAME" x="187.96" y="69.088" size="1.778" layer="95" font="vector" rot="MR0"/>
</instance>
<instance part="GND1" gate="1" x="38.1" y="101.6" smashed="yes" rot="R90">
<attribute name="VALUE" x="40.64" y="99.06" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="FRAME2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="GND4" gate="1" x="115.57" y="119.38" smashed="yes">
<attribute name="VALUE" x="113.03" y="116.84" size="1.778" layer="96"/>
</instance>
<instance part="J3" gate="G$1" x="132.08" y="124.46" smashed="yes" rot="MR180">
<attribute name="NAME" x="131.58" y="119.38" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="127" y="132.08" size="1.778" layer="96" rot="MR180"/>
<attribute name="DIGIKEY" x="132.08" y="124.46" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="MPN" x="132.08" y="124.46" size="1.778" layer="96" rot="MR180" display="off"/>
</instance>
<instance part="F1" gate="G$1" x="78.74" y="99.06" smashed="yes">
<attribute name="NAME" x="73.66" y="101.6" size="1.778" layer="95"/>
<attribute name="VALUE" x="73.66" y="95.25" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="78.74" y="99.06" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="78.74" y="99.06" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="78.74" y="99.06" size="1.778" layer="96" display="off"/>
</instance>
<instance part="F'1" gate="G$1" x="78.74" y="88.9" smashed="yes">
<attribute name="NAME" x="73.66" y="91.44" size="1.778" layer="95"/>
<attribute name="VALUE" x="73.66" y="85.09" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="78.74" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="78.74" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="78.74" y="88.9" size="1.778" layer="96" display="off"/>
</instance>
<instance part="D1" gate="G$1" x="53.34" y="99.06" smashed="yes">
<attribute name="NAME" x="55.88" y="99.5426" size="1.778" layer="95"/>
<attribute name="VALUE" x="50.8" y="94.2086" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="6V" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="121.92" y1="152.4" x2="116.84" y2="152.4" width="0.1524" layer="91"/>
<label x="116.84" y="152.4" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="182.88" y1="152.4" x2="172.72" y2="152.4" width="0.1524" layer="91"/>
<label x="172.72" y="152.4" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="121.92" y1="127" x2="116.84" y2="127" width="0.1524" layer="91"/>
<label x="116.84" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="F1" gate="G$1" pin="2"/>
<wire x1="83.82" y1="99.06" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
<label x="91.44" y="99.06" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SERVO1_PMW" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="121.92" y1="149.86" x2="116.84" y2="149.86" width="0.1524" layer="91"/>
<label x="116.84" y="149.86" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="121.92" y1="147.32" x2="115.57" y2="147.32" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="182.88" y1="147.32" x2="180.34" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="J$1" pin="2"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="121.92" y1="121.92" x2="115.57" y2="121.92" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="J3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<pinref part="J4" gate="J$1" pin="3"/>
<wire x1="35.56" y1="104.14" x2="43.18" y2="104.14" width="0.1524" layer="91"/>
<label x="43.18" y="104.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="OUT+_U1" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="177.8" y1="66.04" x2="170.18" y2="66.04" width="0.1524" layer="91"/>
<label x="170.18" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="OUT-_U2" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="170.18" y1="81.28" x2="177.8" y2="81.28" width="0.1524" layer="91"/>
<label x="170.18" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SERVO2_PMW" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="182.88" y1="149.86" x2="172.72" y2="149.86" width="0.1524" layer="91"/>
<label x="172.72" y="149.86" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SERVO3_PMW" class="0">
<segment>
<wire x1="121.92" y1="124.46" x2="116.84" y2="124.46" width="0.1524" layer="91"/>
<label x="116.84" y="124.46" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="J4" gate="J$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="35.56" y1="99.06" x2="50.8" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="F1" gate="G$1" pin="1"/>
<wire x1="55.88" y1="99.06" x2="71.12" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OUT+_U2" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="177.8" y1="83.82" x2="170.18" y2="83.82" width="0.1524" layer="91"/>
<label x="170.18" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="OUT-_U1" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="177.8" y1="63.5" x2="170.18" y2="63.5" width="0.1524" layer="91"/>
<label x="170.18" y="63.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Driver 1</description>
<plain>
<text x="172.72" y="96.52" size="1.778" layer="97">Need to be close VREG</text>
<text x="68.58" y="86.36" size="1.778" layer="97">Need to be close to VREG</text>
<text x="104.14" y="134.62" size="1.778" layer="97">Need to be close to 12V and VBB</text>
<text x="76.2" y="132.08" size="1.778" layer="97" rot="R180">Need to be close to VBB and 12V</text>
<text x="78.74" y="109.22" size="1.778" layer="97" rot="R180">Need to be close to VCP</text>
<text x="116.84" y="152.4" size="6.4516" layer="97">Driver 1</text>
</plain>
<instances>
<instance part="U1" gate="A" x="132.08" y="99.06" smashed="yes">
<attribute name="NAME" x="126.710140625" y="128.5561" size="2.08686875" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="129.53891875" y="62.7988" size="2.083690625" layer="96" ratio="10" rot="SR0"/>
<attribute name="DIGIKEY" x="132.08" y="99.06" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND9" gate="1" x="109.22" y="68.58" smashed="yes">
<attribute name="VALUE" x="106.68" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="101.6" y="127" smashed="yes">
<attribute name="NAME" x="103.124" y="129.921" size="1.778" layer="95"/>
<attribute name="VALUE" x="103.124" y="124.841" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="101.6" y="127" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="101.6" y="127" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="101.6" y="127" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C6" gate="G$1" x="81.28" y="132.08" smashed="yes" rot="R180">
<attribute name="NAME" x="78.105" y="127" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="90.805" y="135.255" size="1.778" layer="96" rot="R180"/>
<attribute name="DIGIKEY" x="81.28" y="132.08" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MF" x="81.28" y="132.08" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MPN" x="81.28" y="132.08" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="GND13" gate="1" x="81.28" y="137.16" smashed="yes" rot="R180">
<attribute name="VALUE" x="83.82" y="139.7" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND14" gate="1" x="101.6" y="137.16" smashed="yes" rot="R180">
<attribute name="VALUE" x="104.14" y="139.7" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C7" gate="G$1" x="81.28" y="104.14" smashed="yes">
<attribute name="NAME" x="82.804" y="107.061" size="1.778" layer="95"/>
<attribute name="VALUE" x="76.454" y="99.441" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="81.28" y="104.14" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="81.28" y="104.14" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="81.28" y="104.14" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C8" gate="G$1" x="68.58" y="76.2" smashed="yes">
<attribute name="NAME" x="70.104" y="79.121" size="1.778" layer="95"/>
<attribute name="VALUE" x="70.104" y="74.041" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="68.58" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="68.58" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="68.58" y="76.2" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R3" gate="G$1" x="154.94" y="129.54" smashed="yes" rot="R270">
<attribute name="NAME" x="156.4386" y="133.35" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="151.638" y="133.35" size="1.778" layer="96" rot="R270"/>
<attribute name="DIGIKEY" x="154.94" y="129.54" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MPN" x="154.94" y="129.54" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MF" x="154.94" y="129.54" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="R4" gate="G$1" x="96.52" y="50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="98.0186" y="54.61" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="93.218" y="54.61" size="1.778" layer="96" rot="R270"/>
<attribute name="DIGIKEY" x="96.52" y="50.8" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MPN" x="96.52" y="50.8" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MF" x="96.52" y="50.8" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND16" gate="1" x="96.52" y="41.91" smashed="yes">
<attribute name="VALUE" x="93.98" y="39.37" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="172.72" y="81.28" smashed="yes">
<attribute name="NAME" x="174.244" y="84.201" size="1.778" layer="95"/>
<attribute name="VALUE" x="174.244" y="79.121" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="172.72" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="172.72" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="172.72" y="81.28" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND6" gate="1" x="172.72" y="73.66" smashed="yes">
<attribute name="VALUE" x="170.18" y="71.12" size="1.778" layer="96"/>
</instance>
<instance part="FRAME4" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="A" pin="GND"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="114.3" y1="73.66" x2="109.22" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="GND_2"/>
<wire x1="109.22" y1="73.66" x2="109.22" y2="71.12" width="0.1524" layer="91"/>
<wire x1="114.3" y1="76.2" x2="109.22" y2="76.2" width="0.1524" layer="91"/>
<wire x1="109.22" y1="76.2" x2="109.22" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="EPAD"/>
<wire x1="114.3" y1="81.28" x2="109.22" y2="81.28" width="0.1524" layer="91"/>
<wire x1="109.22" y1="81.28" x2="109.22" y2="76.2" width="0.1524" layer="91"/>
<junction x="109.22" y="76.2"/>
<junction x="109.22" y="73.66"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="101.6" y1="132.08" x2="101.6" y2="134.62" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="C6" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="96.52" y1="45.72" x2="96.52" y2="44.45" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="172.72" y1="78.74" x2="172.72" y2="76.2" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<wire x1="114.3" y1="119.38" x2="101.6" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="101.6" y1="124.46" x2="101.6" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VBB"/>
<wire x1="101.6" y1="119.38" x2="81.28" y2="119.38" width="0.1524" layer="91"/>
<junction x="101.6" y="119.38"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="81.28" y1="109.22" x2="81.28" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="81.28" y1="127" x2="81.28" y2="119.38" width="0.1524" layer="91"/>
<junction x="81.28" y="119.38"/>
<label x="71.12" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="81.28" y1="119.38" x2="71.12" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="U1" gate="A" pin="VCP"/>
<wire x1="81.28" y1="101.6" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<wire x1="81.28" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ENABLE_GPIO_U1" class="0">
<segment>
<pinref part="U1" gate="A" pin="ENABLE"/>
<wire x1="114.3" y1="114.3" x2="111.76" y2="114.3" width="0.1524" layer="91"/>
<label x="111.76" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SENSE_U1" class="0">
<segment>
<pinref part="U1" gate="A" pin="SENSE"/>
<wire x1="114.3" y1="111.76" x2="111.76" y2="111.76" width="0.1524" layer="91"/>
<label x="111.76" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="96.52" y1="55.88" x2="96.52" y2="60.96" width="0.1524" layer="91"/>
<label x="96.52" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NFAULT_GPIO_U1" class="0">
<segment>
<pinref part="U1" gate="A" pin="NFAULT"/>
<wire x1="149.86" y1="119.38" x2="154.94" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="154.94" y1="124.46" x2="154.94" y2="119.38" width="0.1524" layer="91"/>
<wire x1="154.94" y1="119.38" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
<junction x="154.94" y="119.38"/>
<label x="157.48" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PHASE_GPIO_U1" class="0">
<segment>
<pinref part="U1" gate="A" pin="PHASE"/>
<wire x1="114.3" y1="104.14" x2="111.76" y2="104.14" width="0.1524" layer="91"/>
<label x="111.76" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NSLEEP_GPIO_U1" class="0">
<segment>
<pinref part="U1" gate="A" pin="NSLEEP"/>
<wire x1="114.3" y1="109.22" x2="111.76" y2="109.22" width="0.1524" layer="91"/>
<label x="111.76" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CP1_U1" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="68.58" y1="81.28" x2="68.58" y2="83.82" width="0.1524" layer="91"/>
<label x="67.31" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="CP1"/>
<wire x1="114.3" y1="99.06" x2="111.76" y2="99.06" width="0.1524" layer="91"/>
<label x="111.76" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CP2_U1" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="68.58" y1="73.66" x2="68.58" y2="71.12" width="0.1524" layer="91"/>
<label x="68.58" y="71.12" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="CP2"/>
<wire x1="114.3" y1="96.52" x2="111.76" y2="96.52" width="0.1524" layer="91"/>
<label x="111.76" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VMCU_3.3V" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="154.94" y1="134.62" x2="154.94" y2="137.16" width="0.1524" layer="91"/>
<label x="154.94" y="137.16" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="OUT+_U1" class="0">
<segment>
<pinref part="U1" gate="A" pin="OUT+"/>
<wire x1="149.86" y1="116.84" x2="157.48" y2="116.84" width="0.1524" layer="91"/>
<label x="157.48" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="OUT-_U1" class="0">
<segment>
<pinref part="U1" gate="A" pin="OUT-"/>
<label x="157.48" y="114.3" size="1.27" layer="95" xref="yes"/>
<wire x1="157.48" y1="114.3" x2="149.86" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAP_U1" class="0">
<segment>
<pinref part="U1" gate="A" pin="VREG"/>
<wire x1="114.3" y1="88.9" x2="111.76" y2="88.9" width="0.1524" layer="91"/>
<label x="111.76" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="172.72" y1="86.36" x2="172.72" y2="91.44" width="0.1524" layer="91"/>
<label x="172.72" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MODE_GPIO_U1" class="0">
<segment>
<wire x1="114.3" y1="106.68" x2="111.76" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="MODE"/>
<label x="111.76" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Driver 2</description>
<plain>
<text x="175.26" y="96.52" size="1.778" layer="97">Need to be close to VREG</text>
<text x="81.28" y="86.36" size="1.778" layer="97">Need to be close to CP1 and CP2</text>
<text x="93.98" y="114.3" size="1.778" layer="97" rot="R180">Need to be close to VCP</text>
<text x="68.58" y="132.08" size="1.778" layer="97" rot="R180">Need to be close to 12V and VBB</text>
<text x="114.3" y="142.24" size="1.778" layer="97">Need to be close to VBB and 12V</text>
<text x="114.3" y="157.48" size="6.4516" layer="97">Driver 2</text>
</plain>
<instances>
<instance part="U2" gate="A" x="142.24" y="101.6" smashed="yes">
<attribute name="NAME" x="136.870140625" y="131.0961" size="2.08686875" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="139.69891875" y="65.3388" size="2.083690625" layer="96" ratio="10" rot="SR0"/>
<attribute name="DIGIKEY" x="142.24" y="101.6" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="142.24" y="101.6" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND8" gate="1" x="119.38" y="73.66" smashed="yes">
<attribute name="VALUE" x="116.84" y="71.12" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="93.98" y="109.22" smashed="yes">
<attribute name="NAME" x="95.504" y="112.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="85.344" y="107.061" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="93.98" y="109.22" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="93.98" y="109.22" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="93.98" y="109.22" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C2" gate="G$1" x="111.76" y="127" smashed="yes">
<attribute name="NAME" x="113.284" y="129.921" size="1.778" layer="95"/>
<attribute name="VALUE" x="113.284" y="124.841" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="111.76" y="127" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="111.76" y="127" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="111.76" y="127" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C3" gate="G$1" x="83.82" y="129.54" smashed="yes" rot="R180">
<attribute name="NAME" x="80.645" y="124.46" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="80.645" y="132.715" size="1.778" layer="96" rot="R180"/>
<attribute name="DIGIKEY" x="83.82" y="129.54" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MF" x="83.82" y="129.54" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MPN" x="83.82" y="129.54" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="C4" gate="G$1" x="81.28" y="76.2" smashed="yes">
<attribute name="NAME" x="82.804" y="79.121" size="1.778" layer="95"/>
<attribute name="VALUE" x="70.104" y="74.041" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="81.28" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="81.28" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="81.28" y="76.2" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND11" gate="1" x="83.82" y="134.62" smashed="yes" rot="R180">
<attribute name="VALUE" x="86.36" y="137.16" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND12" gate="1" x="111.76" y="134.62" smashed="yes" rot="R180">
<attribute name="VALUE" x="114.3" y="137.16" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R1" gate="G$1" x="162.56" y="129.54" smashed="yes" rot="R270">
<attribute name="NAME" x="164.0586" y="133.35" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="159.258" y="133.35" size="1.778" layer="96" rot="R270"/>
<attribute name="DIGIKEY" x="162.56" y="129.54" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MPN" x="162.56" y="129.54" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MF" x="162.56" y="129.54" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="R2" gate="G$1" x="96.52" y="68.58" smashed="yes" rot="R270">
<attribute name="NAME" x="98.0186" y="72.39" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="93.218" y="72.39" size="1.778" layer="96" rot="R270"/>
<attribute name="DIGIKEY" x="96.52" y="68.58" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MPN" x="96.52" y="68.58" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MF" x="96.52" y="68.58" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND15" gate="1" x="96.52" y="58.42" smashed="yes">
<attribute name="VALUE" x="93.98" y="55.88" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="175.26" y="86.36" smashed="yes">
<attribute name="NAME" x="176.784" y="89.281" size="1.778" layer="95"/>
<attribute name="VALUE" x="176.784" y="84.201" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="175.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="175.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="175.26" y="86.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND5" gate="1" x="175.26" y="78.74" smashed="yes">
<attribute name="VALUE" x="172.72" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U2" gate="A" pin="GND"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="119.38" y1="76.2" x2="124.46" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="GND_2"/>
<wire x1="124.46" y1="78.74" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<wire x1="119.38" y1="78.74" x2="119.38" y2="76.2" width="0.1524" layer="91"/>
<junction x="119.38" y="76.2"/>
<pinref part="U2" gate="A" pin="EPAD"/>
<wire x1="124.46" y1="83.82" x2="119.38" y2="83.82" width="0.1524" layer="91"/>
<wire x1="119.38" y1="83.82" x2="119.38" y2="78.74" width="0.1524" layer="91"/>
<junction x="119.38" y="78.74"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="96.52" y1="60.96" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="175.26" y1="83.82" x2="175.26" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CP1_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="CP1"/>
<wire x1="124.46" y1="101.6" x2="119.38" y2="101.6" width="0.1524" layer="91"/>
<label x="119.38" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<label x="81.28" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="81.28" y1="83.82" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CP2_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="CP2"/>
<wire x1="124.46" y1="99.06" x2="119.38" y2="99.06" width="0.1524" layer="91"/>
<label x="119.38" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="81.28" y1="73.66" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<label x="81.28" y="71.12" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U2" gate="A" pin="VCP"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="124.46" y1="93.98" x2="93.98" y2="93.98" width="0.1524" layer="91"/>
<wire x1="93.98" y1="93.98" x2="93.98" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<pinref part="U2" gate="A" pin="VBB"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="124.46" y1="121.92" x2="111.76" y2="121.92" width="0.1524" layer="91"/>
<wire x1="111.76" y1="121.92" x2="93.98" y2="121.92" width="0.1524" layer="91"/>
<wire x1="93.98" y1="121.92" x2="93.98" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="111.76" y1="124.46" x2="111.76" y2="121.92" width="0.1524" layer="91"/>
<junction x="111.76" y="121.92"/>
<wire x1="93.98" y1="121.92" x2="83.82" y2="121.92" width="0.1524" layer="91"/>
<junction x="93.98" y="121.92"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="83.82" y1="121.92" x2="83.82" y2="124.46" width="0.1524" layer="91"/>
<label x="76.2" y="121.92" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="83.82" y1="121.92" x2="76.2" y2="121.92" width="0.1524" layer="91"/>
<junction x="83.82" y="121.92"/>
</segment>
</net>
<net name="NFAULT_GPIO_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="NFAULT"/>
<wire x1="160.02" y1="121.92" x2="162.56" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="162.56" y1="124.46" x2="162.56" y2="121.92" width="0.1524" layer="91"/>
<wire x1="162.56" y1="121.92" x2="167.64" y2="121.92" width="0.1524" layer="91"/>
<junction x="162.56" y="121.92"/>
<label x="167.64" y="121.92" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SENSE_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="SENSE"/>
<wire x1="124.46" y1="114.3" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
<label x="119.38" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="96.52" y1="73.66" x2="96.52" y2="76.2" width="0.1524" layer="91"/>
<label x="96.52" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NSLEEP_GPIO_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="NSLEEP"/>
<wire x1="124.46" y1="111.76" x2="119.38" y2="111.76" width="0.1524" layer="91"/>
<label x="119.38" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ENABLE_GPIO_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="ENABLE"/>
<wire x1="124.46" y1="116.84" x2="119.38" y2="116.84" width="0.1524" layer="91"/>
<label x="119.38" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="OUT+_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="OUT+"/>
<wire x1="160.02" y1="119.38" x2="167.64" y2="119.38" width="0.1524" layer="91"/>
<label x="167.64" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="OUT-_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="OUT-"/>
<wire x1="160.02" y1="116.84" x2="167.64" y2="116.84" width="0.1524" layer="91"/>
<label x="167.64" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CAP_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="VREG"/>
<wire x1="124.46" y1="91.44" x2="119.38" y2="91.44" width="0.1524" layer="91"/>
<label x="119.38" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="175.26" y1="91.44" x2="175.26" y2="93.98" width="0.1524" layer="91"/>
<label x="175.26" y="93.98" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VMCU_3.3V" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="162.56" y1="134.62" x2="162.56" y2="137.16" width="0.1524" layer="91"/>
<label x="162.56" y="137.16" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MODE_GPIO_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="MODE"/>
<wire x1="124.46" y1="109.22" x2="119.38" y2="109.22" width="0.1524" layer="91"/>
<label x="119.38" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PHASE_GPIO_U2" class="0">
<segment>
<pinref part="U2" gate="A" pin="PHASE"/>
<wire x1="124.46" y1="106.68" x2="119.38" y2="106.68" width="0.1524" layer="91"/>
<label x="119.38" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>TOF</description>
<plain>
<text x="104.14" y="139.7" size="6.4516" layer="97">TOF</text>
</plain>
<instances>
<instance part="J7" gate="G$1" x="60.96" y="111.76" smashed="yes">
<attribute name="NAME" x="77.47" y="119.38" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="77.47" y="116.84" size="1.778" layer="96" align="center-left"/>
<attribute name="MPN" x="60.96" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="60.96" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="DIGIKEY" x="60.96" y="111.76" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND17" gate="1" x="58.42" y="96.52" smashed="yes" rot="R270">
<attribute name="VALUE" x="55.88" y="99.06" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J8" gate="G$1" x="60.96" y="86.36" smashed="yes">
<attribute name="NAME" x="77.47" y="93.98" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="77.47" y="91.44" size="1.778" layer="96" align="center-left"/>
<attribute name="MPN" x="60.96" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="60.96" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="DIGIKEY" x="60.96" y="86.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND18" gate="1" x="58.42" y="71.12" smashed="yes" rot="R270">
<attribute name="VALUE" x="55.88" y="73.66" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J9" gate="G$1" x="60.96" y="58.42" smashed="yes">
<attribute name="NAME" x="77.47" y="66.04" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="77.47" y="63.5" size="1.778" layer="96" align="center-left"/>
<attribute name="DIGIKEY" x="60.96" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="60.96" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="60.96" y="58.42" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND19" gate="1" x="58.42" y="40.64" smashed="yes" rot="R270">
<attribute name="VALUE" x="55.88" y="43.18" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J10" gate="G$1" x="134.62" y="111.76" smashed="yes">
<attribute name="NAME" x="151.13" y="119.38" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="151.13" y="116.84" size="1.778" layer="96" align="center-left"/>
<attribute name="DIGIKEY" x="134.62" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="134.62" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="134.62" y="111.76" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND20" gate="1" x="132.08" y="96.52" smashed="yes" rot="R270">
<attribute name="VALUE" x="129.54" y="99.06" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J11" gate="G$1" x="134.62" y="86.36" smashed="yes">
<attribute name="NAME" x="151.13" y="93.98" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="151.13" y="91.44" size="1.778" layer="96" align="center-left"/>
<attribute name="MPN" x="134.62" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="134.62" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="DIGIKEY" x="134.62" y="86.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND21" gate="1" x="132.08" y="71.12" smashed="yes" rot="R270">
<attribute name="VALUE" x="129.54" y="73.66" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="J12" gate="G$1" x="134.62" y="58.42" smashed="yes">
<attribute name="NAME" x="151.13" y="66.04" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="151.13" y="63.5" size="1.778" layer="96" align="center-left"/>
<attribute name="DIGIKEY" x="134.62" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="134.62" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="134.62" y="58.42" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND22" gate="1" x="132.08" y="40.64" smashed="yes" rot="R270">
<attribute name="VALUE" x="129.54" y="43.18" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="FRAME6" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="I2C_SCL0" class="0">
<segment>
<pinref part="J7" gate="G$1" pin="4"/>
<wire x1="60.96" y1="104.14" x2="55.88" y2="104.14" width="0.1524" layer="91"/>
<label x="55.88" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="4"/>
<wire x1="60.96" y1="78.74" x2="55.88" y2="78.74" width="0.1524" layer="91"/>
<label x="55.88" y="78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="4"/>
<wire x1="60.96" y1="50.8" x2="55.88" y2="50.8" width="0.1524" layer="91"/>
<label x="55.88" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="4"/>
<wire x1="134.62" y1="104.14" x2="129.54" y2="104.14" width="0.1524" layer="91"/>
<label x="129.54" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="4"/>
<wire x1="134.62" y1="78.74" x2="129.54" y2="78.74" width="0.1524" layer="91"/>
<label x="129.54" y="78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="4"/>
<wire x1="134.62" y1="50.8" x2="129.54" y2="50.8" width="0.1524" layer="91"/>
<label x="129.54" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VMCU_3.3V" class="0">
<segment>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="60.96" y1="111.76" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<label x="55.88" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="1"/>
<wire x1="60.96" y1="86.36" x2="55.88" y2="86.36" width="0.1524" layer="91"/>
<label x="55.88" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="60.96" y1="58.42" x2="55.88" y2="58.42" width="0.1524" layer="91"/>
<label x="55.88" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="1"/>
<wire x1="134.62" y1="111.76" x2="129.54" y2="111.76" width="0.1524" layer="91"/>
<label x="129.54" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="1"/>
<wire x1="134.62" y1="86.36" x2="129.54" y2="86.36" width="0.1524" layer="91"/>
<label x="129.54" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="1"/>
<wire x1="134.62" y1="58.42" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<label x="129.54" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="60.96" y1="109.22" x2="60.96" y2="96.52" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="2"/>
<wire x1="60.96" y1="83.82" x2="60.96" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="60.96" y1="55.88" x2="60.96" y2="40.64" width="0.1524" layer="91"/>
<pinref part="GND19" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="2"/>
<wire x1="134.62" y1="109.22" x2="134.62" y2="96.52" width="0.1524" layer="91"/>
<pinref part="GND20" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="2"/>
<wire x1="134.62" y1="83.82" x2="134.62" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="2"/>
<wire x1="134.62" y1="55.88" x2="134.62" y2="40.64" width="0.1524" layer="91"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
</net>
<net name="XSHUT1" class="0">
<segment>
<pinref part="J7" gate="G$1" pin="5"/>
<wire x1="60.96" y1="101.6" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<label x="55.88" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="XSHUT2" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="5"/>
<wire x1="60.96" y1="76.2" x2="55.88" y2="76.2" width="0.1524" layer="91"/>
<label x="55.88" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="XSHUT3" class="0">
<segment>
<pinref part="J9" gate="G$1" pin="5"/>
<wire x1="60.96" y1="48.26" x2="55.88" y2="48.26" width="0.1524" layer="91"/>
<label x="55.88" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="XSHUT4" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="5"/>
<wire x1="134.62" y1="101.6" x2="129.54" y2="101.6" width="0.1524" layer="91"/>
<label x="129.54" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="XSHUT5" class="0">
<segment>
<pinref part="J11" gate="G$1" pin="5"/>
<wire x1="134.62" y1="76.2" x2="129.54" y2="76.2" width="0.1524" layer="91"/>
<label x="129.54" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="XSHUT6" class="0">
<segment>
<pinref part="J12" gate="G$1" pin="5"/>
<wire x1="134.62" y1="48.26" x2="129.54" y2="48.26" width="0.1524" layer="91"/>
<label x="129.54" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="I2C_SDA0" class="0">
<segment>
<pinref part="J7" gate="G$1" pin="3"/>
<wire x1="60.96" y1="106.68" x2="55.88" y2="106.68" width="0.1524" layer="91"/>
<label x="55.88" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J8" gate="G$1" pin="3"/>
<wire x1="60.96" y1="81.28" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<label x="55.88" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="3"/>
<wire x1="60.96" y1="53.34" x2="55.88" y2="53.34" width="0.1524" layer="91"/>
<label x="55.88" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="3"/>
<wire x1="134.62" y1="106.68" x2="129.54" y2="106.68" width="0.1524" layer="91"/>
<label x="129.54" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J11" gate="G$1" pin="3"/>
<wire x1="134.62" y1="81.28" x2="129.54" y2="81.28" width="0.1524" layer="91"/>
<label x="129.54" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="3"/>
<wire x1="134.62" y1="53.34" x2="129.54" y2="53.34" width="0.1524" layer="91"/>
<label x="129.54" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
